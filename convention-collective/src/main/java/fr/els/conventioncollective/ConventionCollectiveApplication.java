package fr.els.conventioncollective;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConventionCollectiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConventionCollectiveApplication.class, args);
	}

}

