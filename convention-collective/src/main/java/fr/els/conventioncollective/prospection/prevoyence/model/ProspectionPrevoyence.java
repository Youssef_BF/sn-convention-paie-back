package fr.els.conventioncollective.prospection.prevoyence.model;

public class ProspectionPrevoyence {

	private String champApplicationProfessionnel;

	private String champApplicationgeographique;

	
	public String getChampApplicationProfessionnel() {
		return champApplicationProfessionnel;
	}

	public void setChampApplicationProfessionnel(String champApplicationProfessionnel) {
		this.champApplicationProfessionnel = champApplicationProfessionnel;
	}

	public String getChampApplicationgeographique() {
		return champApplicationgeographique;
	}

	public void setChampApplicationgeographique(String champApplicationgeographique) {
		this.champApplicationgeographique = champApplicationgeographique;
	}
	
	
}
