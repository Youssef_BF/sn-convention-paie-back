package fr.els.conventioncollective.paie.model;

import java.util.Collection;

public class FraisProfessionnelle extends PaieCommon {

	private Collection<CategorieProfessionnelle> categoriesProfessionnelles;
	
	private TypeFraisProfessionnel debutTypeFraisProfessionnel;
	
	private TypeFraisProfessionnel finTypeFraisProfessionnel;

	private ZoneDeFrais debutZoneDeFrais;
	
	private ZoneDeFrais finZoneDeFrais;
	
	private double debutAnciennete;
	
	private double finAnciennete;
	
	private double valeur;
	
	private String commentaire;

	public Collection<CategorieProfessionnelle> getCategoriesProfessionnelles() {
		return categoriesProfessionnelles;
	}

	public void setCategoriesProfessionnelles(Collection<CategorieProfessionnelle> categoriesProfessionnelles) {
		this.categoriesProfessionnelles = categoriesProfessionnelles;
	}

	public TypeFraisProfessionnel getDebutTypeFraisProfessionnel() {
		return debutTypeFraisProfessionnel;
	}

	public void setDebutTypeFraisProfessionnel(TypeFraisProfessionnel debutTypeFraisProfessionnel) {
		this.debutTypeFraisProfessionnel = debutTypeFraisProfessionnel;
	}

	public TypeFraisProfessionnel getFinTypeFraisProfessionnel() {
		return finTypeFraisProfessionnel;
	}

	public void setFinTypeFraisProfessionnel(TypeFraisProfessionnel finTypeFraisProfessionnel) {
		this.finTypeFraisProfessionnel = finTypeFraisProfessionnel;
	}

	public ZoneDeFrais getDebutZoneDeFrais() {
		return debutZoneDeFrais;
	}

	public void setDebutZoneDeFrais(ZoneDeFrais debutZoneDeFrais) {
		this.debutZoneDeFrais = debutZoneDeFrais;
	}

	public ZoneDeFrais getFinZoneDeFrais() {
		return finZoneDeFrais;
	}

	public void setFinZoneDeFrais(ZoneDeFrais finZoneDeFrais) {
		this.finZoneDeFrais = finZoneDeFrais;
	}

	public double getDebutAnciennete() {
		return debutAnciennete;
	}

	public void setDebutAnciennete(double debutAnciennete) {
		this.debutAnciennete = debutAnciennete;
	}

	public double getFinAnciennete() {
		return finAnciennete;
	}

	public void setFinAnciennete(double finAnciennete) {
		this.finAnciennete = finAnciennete;
	}

	public double getValeur() {
		return valeur;
	}

	public void setValeur(double valeur) {
		this.valeur = valeur;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	@Override
	public String toString() {
		return "FraisProfessionnelle [categoriesProfessionnelles=" + categoriesProfessionnelles
				+ ", debutTypeFraisProfessionnel=" + debutTypeFraisProfessionnel + ", finTypeFraisProfessionnel="
				+ finTypeFraisProfessionnel + ", debutZoneDeFrais=" + debutZoneDeFrais + ", finZoneDeFrais="
				+ finZoneDeFrais + ", debutAnciennete=" + debutAnciennete + ", finAnciennete=" + finAnciennete
				+ ", valeur=" + valeur + ", commentaire=" + commentaire + "]";
	}
	
}
