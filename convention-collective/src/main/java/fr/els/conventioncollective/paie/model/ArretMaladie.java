package fr.els.conventioncollective.paie.model;

public class ArretMaladie {

	private String codeArret;
	
	private String libelleArret;

	
	public ArretMaladie() {
	}

	public ArretMaladie(String codeArret, String libelleArret) {
		this.codeArret = codeArret;
		this.libelleArret = libelleArret;
	}

	public String getCodeArret() {
		return codeArret;
	}

	public void setCodeArret(String codeArret) {
		this.codeArret = codeArret;
	}

	public String getLibelleArret() {
		return libelleArret;
	}

	public void setLibelleArret(String libelleArret) {
		this.libelleArret = libelleArret;
	}

	@Override
	public String toString() {
		return "TypeArret [codeArret=" + codeArret + ", libelleArret=" + libelleArret + "]";
	}
	
}
