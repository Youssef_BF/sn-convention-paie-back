package fr.els.conventioncollective.paie.model;

public class ImpactCSG {
	
	private String codeImpact;
	
	private String libelleImpact;

	
	
	public ImpactCSG() {
	}

	public ImpactCSG(String codeImpact, String libelleImpact) {
		this.codeImpact = codeImpact;
		this.libelleImpact = libelleImpact;
	}

	public String getCodeImpact() {
		return codeImpact;
	}

	public void setCodeImpact(String codeImpact) {
		this.codeImpact = codeImpact;
	}

	public String getLibelleImpact() {
		return libelleImpact;
	}

	public void setLibelleImpact(String libelleImpact) {
		this.libelleImpact = libelleImpact;
	}

	@Override
	public String toString() {
		return "ImpactCSG [codeImpact=" + codeImpact + ", libelleImpact=" + libelleImpact + "]";
	}
	

}
