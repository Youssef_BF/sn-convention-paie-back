package fr.els.conventioncollective.paie.model;

import java.util.Collection;


public class MinimaSalaire extends PaieCommon{
	
	private Collection<CategorieProfessionnelle> categoriesProfessionnelles;
	
	private String position;
	
	private double echelon;
	
	private double coefficient;
	
	private double baseFixe;
	
	private double point;
	
	private String formule;
	
	private double salaireMensuelMininalBrut;
	
	private double tauxHoraireMininal;
	
	private double remunerationAnnuelleGarantie;


	public Collection<CategorieProfessionnelle> getCategoriesProfessionnelles() {
		return categoriesProfessionnelles;
	}

	public void setCategoriesProfessionnelles(Collection<CategorieProfessionnelle> categoriesProfessionnelles) {
		this.categoriesProfessionnelles = categoriesProfessionnelles;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public double getEchelon() {
		return echelon;
	}

	public void setEchelon(double echelon) {
		this.echelon = echelon;
	}

	public double getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(double coefficient) {
		this.coefficient = coefficient;
	}

	public double getBaseFixe() {
		return baseFixe;
	}

	public void setBaseFixe(double baseFixe) {
		this.baseFixe = baseFixe;
	}

	public double getPoint() {
		return point;
	}

	public void setPoint(double point) {
		this.point = point;
	}

	public String getFormule() {
		return formule;
	}

	public void setFormule(String formule) {
		this.formule = formule;
	}

	public double getSalaireMensuelMininalBrut() {
		return salaireMensuelMininalBrut;
	}

	public void setSalaireMensuelMininalBrut(double salaireMensuelMininalBrut) {
		this.salaireMensuelMininalBrut = salaireMensuelMininalBrut;
	}

	public double getTauxHoraireMininal() {
		return tauxHoraireMininal;
	}

	public void setTauxHoraireMininal(double tauxHoraireMininal) {
		this.tauxHoraireMininal = tauxHoraireMininal;
	}

	public double getRemunerationAnnuelleGarantie() {
		return remunerationAnnuelleGarantie;
	}

	public void setRemunerationAnnuelleGarantie(double remunerationAnnuelleGarantie) {
		this.remunerationAnnuelleGarantie = remunerationAnnuelleGarantie;
	}

	@Override
	public String toString() {
		return "MinimaSalaire [categoriesProfessionnelles=" + categoriesProfessionnelles + ", position=" + position + ", echelon=" + echelon
				+ ", coefficient=" + coefficient + ", baseFixe=" + baseFixe + ", point=" + point + ", formule="
				+ formule + ", salaireMensuelMininalBrut=" + salaireMensuelMininalBrut + ", tauxHoraireMininal="
				+ tauxHoraireMininal + ", remunerationAnnuelleGarantie=" + remunerationAnnuelleGarantie + "]";
	}
	
}
