package fr.els.conventioncollective.paie.model;

import java.util.Collection;

public class CongesDivers extends PaieCommon{

	private Collection<CategorieProfessionnelle> categorieProfessionnelles;

	private Conge debutTypeConge;

	private Conge finTypeConge;
	
	private double ancienneteMin;
	
	private double ancienneteMax;
	
	private double nombreJours;
	
	private String paiementConge;
	
	private Acquisition acquisition;
	
	private Decompte decompte;
	
	private String commentaire;

	public Collection<CategorieProfessionnelle> getCategorieProfessionnelles() {
		return categorieProfessionnelles;
	}

	public void setCategorieProfessionnelles(Collection<CategorieProfessionnelle> categorieProfessionnelles) {
		this.categorieProfessionnelles = categorieProfessionnelles;
	}

	public Conge getDebutTypeConge() {
		return debutTypeConge;
	}

	public void setDebutTypeConge(Conge debutTypeConge) {
		this.debutTypeConge = debutTypeConge;
	}

	public Conge getFinTypeConge() {
		return finTypeConge;
	}

	public void setFinTypeConge(Conge finTypeConge) {
		this.finTypeConge = finTypeConge;
	}

	public double getAncienneteMin() {
		return ancienneteMin;
	}

	public void setAncienneteMin(double ancienneteMin) {
		this.ancienneteMin = ancienneteMin;
	}

	public double getAncienneteMax() {
		return ancienneteMax;
	}

	public void setAncienneteMax(double ancienneteMax) {
		this.ancienneteMax = ancienneteMax;
	}

	public double getNombreJours() {
		return nombreJours;
	}

	public void setNombreJours(double nombreJours) {
		this.nombreJours = nombreJours;
	}

	public String getPaiementConge() {
		return paiementConge;
	}

	public void setPaiementConge(String paiementConge) {
		this.paiementConge = paiementConge;
	}

	public Acquisition getAcquisition() {
		return acquisition;
	}

	public void setAcquisition(Acquisition acquisition) {
		this.acquisition = acquisition;
	}

	public Decompte getDecompte() {
		return decompte;
	}

	public void setDecompte(Decompte decompte) {
		this.decompte = decompte;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	@Override
	public String toString() {
		return "CongesDivers [categorieProfessionnelles=" + categorieProfessionnelles + ", debutTypeConge="
				+ debutTypeConge + ", finTypeConge=" + finTypeConge + ", ancienneteMin=" + ancienneteMin
				+ ", ancienneteMax=" + ancienneteMax + ", nombreJours=" + nombreJours + ", paiementConge="
				+ paiementConge + ", acquisition=" + acquisition + ", decompte=" + decompte + ", commentaire="
				+ commentaire + "]";
	}
	
}
