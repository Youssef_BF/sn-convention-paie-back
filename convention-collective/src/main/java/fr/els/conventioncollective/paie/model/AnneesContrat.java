package fr.els.conventioncollective.paie.model;

public class AnneesContrat {

	private double anneeDebut;
	
	private double anneeFin;

	
	public AnneesContrat() {
	}

	public AnneesContrat(double anneeDebut, double anneeFin) {
		this.anneeDebut = anneeDebut;
		this.anneeFin = anneeFin;
	}


	public double getAnneeDebut() {
		return anneeDebut;
	}

	public void setAnneeDebut(double anneeDebut) {
		this.anneeDebut = anneeDebut;
	}

	public double getAnneeFin() {
		return anneeFin;
	}

	public void setAnneeFin(double anneeFin) {
		this.anneeFin = anneeFin;
	}

	
		
}
