package fr.els.conventioncollective.paie.model;

public class TypePrime {

	private int codePrime;
	
	private String libellePrime;

	
	public TypePrime() {
	}

	public TypePrime(int codePrime, String libellePrime) {
		this.codePrime = codePrime;
		this.libellePrime = libellePrime;
	}

	public int getCodePrime() {
		return codePrime;
	}

	public void setCodePrime(int codePrime) {
		this.codePrime = codePrime;
	}

	public String getLibellePrime() {
		return libellePrime;
	}

	public void setLibellePrime(String libellePrime) {
		this.libellePrime = libellePrime;
	}

	@Override
	public String toString() {
		return "TypePrime [codePrime=" + codePrime + ", libellePrime=" + libellePrime + "]";
	}
	
	
}
