package fr.els.conventioncollective.paie.model;

public class EvolutionDroit {

	private String code;
	
	private String libelle;

	
	public EvolutionDroit() {
	}

	public EvolutionDroit(String code, String libelle) {
		this.code = code;
		this.libelle = libelle;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@Override
	public String toString() {
		return "EvolutionDroit [code=" + code + ", libelle=" + libelle + "]";
	}
	
	
}
