package fr.els.conventioncollective.paie.model;

public class Anciennete {

	private double debutAnciennete;

	private double finAnciennete;
	
	private TypeAnciennete typeAnciennete;
	
	private String specifiteAnciennete;

	
	public Anciennete() {
	}

	public Anciennete(double debutAnciennete, double finAnciennete, TypeAnciennete typeAnciennete,
			String specifiteAnciennete) {
		this.debutAnciennete = debutAnciennete;
		this.finAnciennete = finAnciennete;
		this.typeAnciennete = typeAnciennete;
		this.specifiteAnciennete = specifiteAnciennete;
	}

	public double getDebutAnciennete() {
		return debutAnciennete;
	}

	public void setDebutAnciennete(double debutAnciennete) {
		this.debutAnciennete = debutAnciennete;
	}

	public double getFinAnciennete() {
		return finAnciennete;
	}

	public void setFinAnciennete(double finAnciennete) {
		this.finAnciennete = finAnciennete;
	}

	public TypeAnciennete getTypeAnciennete() {
		return typeAnciennete;
	}

	public void setTypeAnciennete(TypeAnciennete typeAnciennete) {
		this.typeAnciennete = typeAnciennete;
	}

	public String getSpecifiteAnciennete() {
		return specifiteAnciennete;
	}

	public void setSpecifiteAnciennete(String specifiteAnciennete) {
		this.specifiteAnciennete = specifiteAnciennete;
	}
	
	
}
