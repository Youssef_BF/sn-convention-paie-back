package fr.els.conventioncollective.paie.model;

public class NiveauEducationNatioanle {

	private String codeNiveau;
	
	private String libelleNiveau;

	public NiveauEducationNatioanle() {
	}

	public NiveauEducationNatioanle(String codeNiveau, String libelleNiveau) {
		this.codeNiveau = codeNiveau;
		this.libelleNiveau = libelleNiveau;
	}

	public String getCodeNiveau() {
		return codeNiveau;
	}

	public void setCodeNiveau(String codeNiveau) {
		this.codeNiveau = codeNiveau;
	}

	public String getLibelleNiveau() {
		return libelleNiveau;
	}

	public void setLibelleNiveau(String libelleNiveau) {
		this.libelleNiveau = libelleNiveau;
	}

	@Override
	public String toString() {
		return "NiveauEducationNatioanle [codeNiveau=" + codeNiveau + ", libelleNiveau=" + libelleNiveau + "]";
	}

}
