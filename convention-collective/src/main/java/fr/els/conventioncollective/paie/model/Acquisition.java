package fr.els.conventioncollective.paie.model;

public class Acquisition {

	private String code;

	private String lielle;

	public Acquisition() {
	}

	public Acquisition(String code, String lielle) {
		this.code = code;
		this.lielle = lielle;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLielle() {
		return lielle;
	}

	public void setLielle(String lielle) {
		this.lielle = lielle;
	}

	@Override
	public String toString() {
		return "Acquisition [code=" + code + ", lielle=" + lielle + "]";
	}

}
