package fr.els.conventioncollective.paie.model;

public class AgeApprenti {

	private double debutAge;
	
	private double finAge;
	
	
	public AgeApprenti() {
	}

	
	public AgeApprenti(double debutAge, double finAge) {
		this.debutAge = debutAge;
		this.finAge = finAge;
	}


	public double getDebutAge() {
		return debutAge;
	}

	public void setDebutAge(double debutAge) {
		this.debutAge = debutAge;
	}

	public double getFinAge() {
		return finAge;
	}

	public void setFinAge(double finAge) {
		this.finAge = finAge;
	}

	@Override
	public String toString() {
		return "AgeApprenti [debutAge=" + debutAge + ", finAge=" + finAge + "]";
	}
	
	
}
