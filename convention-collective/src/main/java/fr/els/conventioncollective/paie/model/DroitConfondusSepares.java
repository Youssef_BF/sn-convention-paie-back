package fr.els.conventioncollective.paie.model;

public class DroitConfondusSepares {

	private String codeDroit;
	
	private String libelleDroit;


	public DroitConfondusSepares() {
	}

	public DroitConfondusSepares(String codeDroit, String libelleDroit) {
		this.codeDroit = codeDroit;
		this.libelleDroit = libelleDroit;
	}

	public String getCodeDroit() {
		return codeDroit;
	}

	public void setCodeDroit(String codeDroit) {
		this.codeDroit = codeDroit;
	}

	public String getLibelleDroit() {
		return libelleDroit;
	}

	public void setLibelleDroit(String libelleDroit) {
		this.libelleDroit = libelleDroit;
	}

	@Override
	public String toString() {
		return "DroitConfondusSepares [codeDroit=" + codeDroit + ", libelleDroit=" + libelleDroit + "]";
	}

	
}
