package fr.els.conventioncollective.paie.model;

public class ContratPro extends PaieCommon {


	private double anneeDebut;
	
	private double anneeFin;

	private double ageMin;
	
	private double ageMax;
	
	private NiveauEducationNatioanle niveauDebut;

	private NiveauEducationNatioanle niveauFin;
	
	private double pourcentageSMIC;
	
	private double pourcentageMinimaConventionnel;

	private int idccParent;

	
	public double getAnneeDebut() {
		return anneeDebut;
	}

	public void setAnneeDebut(double anneeDebut) {
		this.anneeDebut = anneeDebut;
	}

	public double getAnneeFin() {
		return anneeFin;
	}

	public void setAnneeFin(double anneeFin) {
		this.anneeFin = anneeFin;
	}

	public double getAgeMin() {
		return ageMin;
	}

	public void setAgeMin(double ageMin) {
		this.ageMin = ageMin;
	}

	public double getAgeMax() {
		return ageMax;
	}

	public void setAgeMax(double ageMax) {
		this.ageMax = ageMax;
	}

	public NiveauEducationNatioanle getNiveauDebut() {
		return niveauDebut;
	}

	public void setNiveauDebut(NiveauEducationNatioanle niveauDebut) {
		this.niveauDebut = niveauDebut;
	}

	public NiveauEducationNatioanle getNiveauFin() {
		return niveauFin;
	}

	public void setNiveauFin(NiveauEducationNatioanle niveauFin) {
		this.niveauFin = niveauFin;
	}

	public double getPourcentageSMIC() {
		return pourcentageSMIC;
	}

	public void setPourcentageSMIC(double pourcentageSMIC) {
		this.pourcentageSMIC = pourcentageSMIC;
	}

	public double getPourcentageMinimaConventionnel() {
		return pourcentageMinimaConventionnel;
	}

	public void setPourcentageMinimaConventionnel(double pourcentageMinimaConventionnel) {
		this.pourcentageMinimaConventionnel = pourcentageMinimaConventionnel;
	}

	public int getIdccParent() {
		return idccParent;
	}

	public void setIdccParent(int idccParent) {
		this.idccParent = idccParent;
	}

	@Override
	public String toString() {
		return "ContratPro [anneeDebut=" + anneeDebut + ", anneeFin=" + anneeFin + ", ageMin=" + ageMin + ", ageMax="
				+ ageMax + ", niveauDebut=" + niveauDebut + ", niveauFin=" + niveauFin + ", pourcentageSMIC="
				+ pourcentageSMIC + ", pourcentageMinimaConventionnel=" + pourcentageMinimaConventionnel
				+ ", idccParent=" + idccParent + "]";
	}

}
