package fr.els.conventioncollective.paie.model;

public class MontantPrime {

	private double montant;
	
	private MoisVersementPrime moisDeVersement;

	
	public MontantPrime() {
	}

	public MontantPrime(double montant, MoisVersementPrime moisDeVersement) {
		this.montant = montant;
		this.moisDeVersement = moisDeVersement;
	}

	public double getMontant() {
		return montant;
	}

	public void setMontant(double montant) {
		this.montant = montant;
	}

	public MoisVersementPrime getMoisDeVersement() {
		return moisDeVersement;
	}

	public void setMoisDeVersement(MoisVersementPrime moisDeVersement) {
		this.moisDeVersement = moisDeVersement;
	}

	@Override
	public String toString() {
		return "MontantPrime [montant=" + montant + ", moisDeVersement=" + moisDeVersement + "]";
	}

}
