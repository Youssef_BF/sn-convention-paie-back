package fr.els.conventioncollective.paie.model;

import java.util.Collection;

public class MaladieMaintienSalaire extends PaieCommon {

	private Collection<CategorieProfessionnelle> categorieProfessionnelles;

	private ArretMaladie debutTypeArret;

	private ArretMaladie finTypeArret;

	private double debutAge;

	private double finAge;

	private double debutDureeArret;
	
	private double finDureeArret;
	
	private Anciennete anciennete1;

	private Anciennete anciennete2;
	
	private int minEnfantAttendu;

	private int maxEnfantAttendu;
	
	private int minRangEnfant;

	private int maxRangEnfant;
	
	private double nombreJours;
	
	private MaintienSalaire maintienSalaire1;

	private MaintienSalaire maintienSalaire2;

	private int idccParent;

	
	public Collection<CategorieProfessionnelle> getCategorieProfessionnelles() {
		return categorieProfessionnelles;
	}

	public void setCategorieProfessionnelles(Collection<CategorieProfessionnelle> categorieProfessionnelles) {
		this.categorieProfessionnelles = categorieProfessionnelles;
	}

	public ArretMaladie getDebutTypeArret() {
		return debutTypeArret;
	}

	public void setDebutTypeArret(ArretMaladie debutTypeArret) {
		this.debutTypeArret = debutTypeArret;
	}

	public ArretMaladie getFinTypeArret() {
		return finTypeArret;
	}

	public void setFinTypeArret(ArretMaladie finTypeArret) {
		this.finTypeArret = finTypeArret;
	}

	public double getDebutAge() {
		return debutAge;
	}

	public void setDebutAge(double debutAge) {
		this.debutAge = debutAge;
	}

	public double getFinAge() {
		return finAge;
	}

	public void setFinAge(double finAge) {
		this.finAge = finAge;
	}

	public double getDebutDureeArret() {
		return debutDureeArret;
	}

	public void setDebutDureeArret(double debutDureeArret) {
		this.debutDureeArret = debutDureeArret;
	}

	public double getFinDureeArret() {
		return finDureeArret;
	}

	public void setFinDureeArret(double finDureeArret) {
		this.finDureeArret = finDureeArret;
	}

	public Anciennete getAnciennete1() {
		return anciennete1;
	}

	public void setAnciennete1(Anciennete anciennete1) {
		this.anciennete1 = anciennete1;
	}

	public Anciennete getAnciennete2() {
		return anciennete2;
	}

	public void setAnciennete2(Anciennete anciennete2) {
		this.anciennete2 = anciennete2;
	}

	public int getMinEnfantAttendu() {
		return minEnfantAttendu;
	}

	public void setMinEnfantAttendu(int minEnfantAttendu) {
		this.minEnfantAttendu = minEnfantAttendu;
	}

	public int getMaxEnfantAttendu() {
		return maxEnfantAttendu;
	}

	public void setMaxEnfantAttendu(int maxEnfantAttendu) {
		this.maxEnfantAttendu = maxEnfantAttendu;
	}

	public int getMinRangEnfant() {
		return minRangEnfant;
	}

	public void setMinRangEnfant(int minRangEnfant) {
		this.minRangEnfant = minRangEnfant;
	}

	public int getMaxRangEnfant() {
		return maxRangEnfant;
	}

	public void setMaxRangEnfant(int maxRangEnfant) {
		this.maxRangEnfant = maxRangEnfant;
	}

	public double getNombreJours() {
		return nombreJours;
	}

	public void setNombreJours(double nombreJours) {
		this.nombreJours = nombreJours;
	}

	public MaintienSalaire getMaintienSalaire1() {
		return maintienSalaire1;
	}

	public void setMaintienSalaire1(MaintienSalaire maintienSalaire1) {
		this.maintienSalaire1 = maintienSalaire1;
	}

	public MaintienSalaire getMaintienSalaire2() {
		return maintienSalaire2;
	}

	public void setMaintienSalaire2(MaintienSalaire maintienSalaire2) {
		this.maintienSalaire2 = maintienSalaire2;
	}

	public int getIdccParent() {
		return idccParent;
	}

	public void setIdccParent(int idccParent) {
		this.idccParent = idccParent;
	}

	@Override
	public String toString() {
		return "MaladieMaintienSalaire [categorieProfessionnelles=" + categorieProfessionnelles + ", debutTypeArret="
				+ debutTypeArret + ", finTypeArret=" + finTypeArret + ", debutAge=" + debutAge + ", finAge=" + finAge
				+ ", debutDureeArret=" + debutDureeArret + ", finDureeArret=" + finDureeArret + ", anciennete1="
				+ anciennete1 + ", anciennete2=" + anciennete2 + ", minEnfantAttendu=" + minEnfantAttendu
				+ ", maxEnfantAttendu=" + maxEnfantAttendu + ", minRangEnfant=" + minRangEnfant + ", maxRangEnfant="
				+ maxRangEnfant + ", nombreJours=" + nombreJours + ", maintienSalaire1=" + maintienSalaire1
				+ ", maintienSalaire2=" + maintienSalaire2 + ", idccParent=" + idccParent + "]";
	}
	
}
