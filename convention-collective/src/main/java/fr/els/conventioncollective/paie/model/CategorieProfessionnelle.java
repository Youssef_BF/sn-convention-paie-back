package fr.els.conventioncollective.paie.model;

public class CategorieProfessionnelle {

	private String codeCategorie;

	private String libelleCategorie;

	public CategorieProfessionnelle() {
	}

	public CategorieProfessionnelle(String codeCategorie, String libelleCategorie) {
		this.codeCategorie = codeCategorie;
		this.libelleCategorie = libelleCategorie;
	}

	public String getCodeCategorie() {
		return codeCategorie;
	}

	public void setCodeCategorie(String codeCategorie) {
		this.codeCategorie = codeCategorie;
	}

	public String getLibelleCategorie() {
		return libelleCategorie;
	}

	public void setLibelleCategorie(String libelleCategorie) {
		this.libelleCategorie = libelleCategorie;
	}

	@Override
	public String toString() {
		return "CategorieProfessionnelle [codeCategorie=" + codeCategorie + ", libelleCategorie=" + libelleCategorie
				+ "]";
	}

}
