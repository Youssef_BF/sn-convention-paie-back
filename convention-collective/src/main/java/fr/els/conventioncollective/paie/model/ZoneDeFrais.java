package fr.els.conventioncollective.paie.model;

public class ZoneDeFrais {

	private String codeZone;
	
	private String libelleZone;

	
	public ZoneDeFrais() {
	}

	public ZoneDeFrais(String codeZone, String libelleZone) {
		this.codeZone = codeZone;
		this.libelleZone = libelleZone;
	}

	public String getCodeZone() {
		return codeZone;
	}

	public void setCodeZone(String codeZone) {
		this.codeZone = codeZone;
	}

	public String getLibelleZone() {
		return libelleZone;
	}

	public void setLibelleZone(String libelleZone) {
		this.libelleZone = libelleZone;
	}

	@Override
	public String toString() {
		return "ZoneDeFrais [codeZone=" + codeZone + ", libelleZone=" + libelleZone + "]";
	}
	
	
}
