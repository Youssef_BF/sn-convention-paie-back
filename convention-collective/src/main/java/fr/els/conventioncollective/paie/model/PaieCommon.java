package fr.els.conventioncollective.paie.model;

import java.util.Collection;
import java.util.Date;

public class PaieCommon {

	protected Date dateFinRattachement;

	protected Date dateDebutValidite;

	protected Date dateFinValidite;

	private String secteurActivite;

	protected Collection<ZoneGeographique> zonesGeographiques;

	protected String adherent;

	protected String donneesConventionnellesNonModelisees;

	public Date getDateFinRattachement() {
		return dateFinRattachement;
	}

	public void setDateFinRattachement(Date dateFinRattachement) {
		this.dateFinRattachement = dateFinRattachement;
	}

	public Date getDateDebutValidite() {
		return dateDebutValidite;
	}

	public void setDateDebutValidite(Date dateDebutValidite) {
		this.dateDebutValidite = dateDebutValidite;
	}

	public Date getDateFinValidite() {
		return dateFinValidite;
	}

	public void setDateFinValidite(Date dateFinValidite) {
		this.dateFinValidite = dateFinValidite;
	}

	public String getSecteurActivite() {
		return secteurActivite;
	}

	public void setSecteurActivite(String secteurActivite) {
		this.secteurActivite = secteurActivite;
	}

	public Collection<ZoneGeographique> getZonesGeographiques() {
		return zonesGeographiques;
	}

	public void setZonesGeographiques(Collection<ZoneGeographique> zonesGeographiques) {
		this.zonesGeographiques = zonesGeographiques;
	}

	public String getAdherent() {
		return adherent;
	}

	public void setAdherent(String adherent) {
		this.adherent = adherent;
	}

	public String getDonneesConventionnellesNonModelisees() {
		return donneesConventionnellesNonModelisees;
	}

	public void setDonneesConventionnellesNonModelisees(String donneesConventionnellesNonModelisees) {
		this.donneesConventionnellesNonModelisees = donneesConventionnellesNonModelisees;
	}

	@Override
	public String toString() {
		return "PaieCommon [dateFinRattachement=" + dateFinRattachement + ", dateDebutValidite=" + dateDebutValidite
				+ ", dateFinValidite=" + dateFinValidite + ", secteurActivite=" + secteurActivite
				+ ", zonesGeographiques=" + zonesGeographiques + ", adherent=" + adherent
				+ ", donneesConventionnellesNonModelisees=" + donneesConventionnellesNonModelisees + "]";
	}

}
