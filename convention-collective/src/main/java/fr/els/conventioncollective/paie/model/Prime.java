package fr.els.conventioncollective.paie.model;

import java.util.Collection;

public class Prime extends PaieCommon {

	private Collection<CategorieProfessionnelle> categorieProfessionnelles;
	
	private Collection<TypePrime> typePrimes;
	
	private double debutAnciennete;

	private double finAnciennete;
	
	private MontantPrime montantPrime1;

	private MontantPrime montantPrime2;
	
	private String baseCalcul;
	
	private String commentaire;
	

	public Collection<CategorieProfessionnelle> getCategorieProfessionnelles() {
		return categorieProfessionnelles;
	}

	public void setCategorieProfessionnelles(Collection<CategorieProfessionnelle> categorieProfessionelles) {
		this.categorieProfessionnelles = categorieProfessionelles;
	}

	public Collection<TypePrime> getTypePrimes() {
		return typePrimes;
	}

	public void setTypePrimes(Collection<TypePrime> typePrimes) {
		this.typePrimes = typePrimes;
	}

	public double getDebutAnciennete() {
		return debutAnciennete;
	}

	public void setDebutAnciennete(double debutAnciennete) {
		this.debutAnciennete = debutAnciennete;
	}

	public double getFinAnciennete() {
		return finAnciennete;
	}

	public void setFinAnciennete(double finAnciennete) {
		this.finAnciennete = finAnciennete;
	}

	public MontantPrime getMontantPrime1() {
		return montantPrime1;
	}

	public void setMontantPrime1(MontantPrime montantPrime1) {
		this.montantPrime1 = montantPrime1;
	}

	public MontantPrime getMontantPrime2() {
		return montantPrime2;
	}

	public void setMontantPrime2(MontantPrime montantPrime2) {
		this.montantPrime2 = montantPrime2;
	}

	public String getBaseCalcul() {
		return baseCalcul;
	}

	public void setBaseCalcul(String baseCalcul) {
		this.baseCalcul = baseCalcul;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	@Override
	public String toString() {
		return "Prime [categorieProfessionnelles=" + categorieProfessionnelles + ", typePrimes=" + typePrimes
				+ ", debutAnciennete=" + debutAnciennete + ", finAnciennete=" + finAnciennete + ", montantPrime1="
				+ montantPrime1 + ", montantPrime2=" + montantPrime2 + ", baseCalcul=" + baseCalcul + ", commentaire="
				+ commentaire + "]";
	}
	
	
}
