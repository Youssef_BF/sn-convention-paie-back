package fr.els.conventioncollective.paie.model;

public class RegulAuNet {

	private String code;

	private String libelle;


	public RegulAuNet() {
	}

	public RegulAuNet(String code, String libelle) {
		this.code = code;
		this.libelle = libelle;
	}


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@Override
	public String toString() {
		return "RegulAuNet [code=" + code + ", libelle=" + libelle + "]";
	}
	
	
}
