package fr.els.conventioncollective.paie.model;

import java.util.Collection;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class PaieCC {

	@Id
	private String idCC;
	
	private int idccIndexe;

	private String nomCC;
	
	private Collection<MinimaSalaire> minimaSalaire;

	private Collection<FraisProfessionnelle> fraisProfessionnelles;
	
	private Collection<Apprenti> apprenti;

	private Collection<ContratPro> contratPro;

	private Collection<MaladieMaintienSalaire> maladieMaintienSalaire;

	private Collection<MaladieRegle> maladieRegle;

	private Collection<Carence> carence;

	private Collection<CongesDivers> congesDivers;
	
	private Collection<Prime> prime;

	public String getIdCC() {
		return idCC;
	}

	public void setIdCC(String idCC) {
		this.idCC = idCC;
	}

	public int getIdccIndexe() {
		return idccIndexe;
	}

	public void setIdccIndexe(int idccIndexe) {
		this.idccIndexe = idccIndexe;
	}

	public String getNomCC() {
		return nomCC;
	}

	public void setNomCC(String nomCC) {
		this.nomCC = nomCC;
	}

	public Collection<MinimaSalaire> getMinimaSalaire() {
		return minimaSalaire;
	}

	public void setMinimaSalaire(Collection<MinimaSalaire> minimaSalaire) {
		this.minimaSalaire = minimaSalaire;
	}
	
	public Collection<FraisProfessionnelle> getFraisProfessionnelles() {
		return fraisProfessionnelles;
	}

	public void setFraisProfessionnelles(Collection<FraisProfessionnelle> fraisProfessionnelles) {
		this.fraisProfessionnelles = fraisProfessionnelles;
	}

	public Collection<Apprenti> getApprenti() {
		return apprenti;
	}

	public void setApprenti(Collection<Apprenti> apprenti) {
		this.apprenti = apprenti;
	}

	public Collection<ContratPro> getContratPro() {
		return contratPro;
	}

	public void setContratPro(Collection<ContratPro> contratPro) {
		this.contratPro = contratPro;
	}

	public Collection<MaladieMaintienSalaire> getMaladieMaintienSalaire() {
		return maladieMaintienSalaire;
	}

	public void setMaladieMaintienSalaire(Collection<MaladieMaintienSalaire> maladieMaintienSalaire) {
		this.maladieMaintienSalaire = maladieMaintienSalaire;
	}

	public Collection<MaladieRegle> getMaladieRegle() {
		return maladieRegle;
	}

	public void setMaladieRegle(Collection<MaladieRegle> maladieRegle) {
		this.maladieRegle = maladieRegle;
	}

	public Collection<Carence> getCarence() {
		return carence;
	}

	public void setCarence(Collection<Carence> carence) {
		this.carence = carence;
	}

	public Collection<CongesDivers> getCongesDivers() {
		return congesDivers;
	}

	public void setCongesDivers(Collection<CongesDivers> congesDivers) {
		this.congesDivers = congesDivers;
	}

	public Collection<Prime> getPrime() {
		return prime;
	}

	public void setPrime(Collection<Prime> prime) {
		this.prime = prime;
	}

	@Override
	public String toString() {
		return "PaieCC [idCC=" + idCC + ", idccIndexe=" + idccIndexe + ", nomCC=" + nomCC + ", minimaSalaire="
				+ minimaSalaire + ", fraisProfessionnelles=" + fraisProfessionnelles + ", apprenti=" + apprenti
				+ ", contratPro=" + contratPro + ", maladieMaintienSalaire=" + maladieMaintienSalaire
				+ ", maladieRegle=" + maladieRegle + ", carence=" + carence + ", congesDivers=" + congesDivers
				+ ", prime=" + prime + "]";
	}

}
