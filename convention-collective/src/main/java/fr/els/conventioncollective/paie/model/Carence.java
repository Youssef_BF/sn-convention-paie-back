package fr.els.conventioncollective.paie.model;

import java.util.Collection;

public class Carence extends PaieCommon{

	private Collection<CategorieProfessionnelle> categorieProfessionnelles;
	
	private ArretMaladie debutTypeArret;

	private ArretMaladie finTypeArret;
	
	private double debutDureeArret;
	
	private double finDureeArret;

	private double debutAnciennete;

	private double finAnciennete;
	
	private double nombre;
	
	private String particularites;
	

	public Collection<CategorieProfessionnelle> getCategorieProfessionnelles() {
		return categorieProfessionnelles;
	}

	public void setCategorieProfessionnelles(Collection<CategorieProfessionnelle> categorieProfessionnelles) {
		this.categorieProfessionnelles = categorieProfessionnelles;
	}

	public ArretMaladie getDebutTypeArret() {
		return debutTypeArret;
	}

	public void setDebutTypeArret(ArretMaladie debutTypeArret) {
		this.debutTypeArret = debutTypeArret;
	}

	public ArretMaladie getFinTypeArret() {
		return finTypeArret;
	}

	public void setFinTypeArret(ArretMaladie finTypeArret) {
		this.finTypeArret = finTypeArret;
	}

	public double getDebutDureeArret() {
		return debutDureeArret;
	}

	public void setDebutDureeArret(double debutDureeArret) {
		this.debutDureeArret = debutDureeArret;
	}

	public double getFinDureeArret() {
		return finDureeArret;
	}

	public void setFinDureeArret(double finDureeArret) {
		this.finDureeArret = finDureeArret;
	}

	public double getDebutAnciennete() {
		return debutAnciennete;
	}

	public void setDebutAnciennete(double debutAnciennete) {
		this.debutAnciennete = debutAnciennete;
	}

	public double getFinAnciennete() {
		return finAnciennete;
	}

	public void setFinAnciennete(double finAnciennete) {
		this.finAnciennete = finAnciennete;
	}

	public double getNombre() {
		return nombre;
	}

	public void setNombre(double nombre) {
		this.nombre = nombre;
	}

	public String getParticularites() {
		return particularites;
	}

	public void setParticularites(String particularites) {
		this.particularites = particularites;
	}

	@Override
	public String toString() {
		return "Carence [categorieProfessionnelles=" + categorieProfessionnelles + ", debutTypeArret=" + debutTypeArret
				+ ", finTypeArret=" + finTypeArret + ", debutDureeArret=" + debutDureeArret + ", finDureeArret="
				+ finDureeArret + ", debutAnciennete=" + debutAnciennete + ", finAnciennete=" + finAnciennete
				+ ", nombre=" + nombre + ", particularites=" + particularites + "]";
	}

}
