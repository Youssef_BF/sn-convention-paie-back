package fr.els.conventioncollective.paie.model;

public class ZoneGeographique {

	private String codeZoneGeographique;
	
	private String libelleZoneGeographique;

	
	public ZoneGeographique() {
	}

	public ZoneGeographique(String codeZoneGeographique, String libelleZoneGeographique) {
		this.codeZoneGeographique = codeZoneGeographique;
		this.libelleZoneGeographique = libelleZoneGeographique;
	}

	public String getCodeZoneGeographique() {
		return codeZoneGeographique;
	}

	public void setCodeZoneGeographique(String codeZoneGeographique) {
		this.codeZoneGeographique = codeZoneGeographique;
	}

	public String getLibelleZoneGeographique() {
		return libelleZoneGeographique;
	}

	public void setLibelleZoneGeographique(String libelleZoneGeographique) {
		this.libelleZoneGeographique = libelleZoneGeographique;
	}

	@Override
	public String toString() {
		return "ZoneGeographique [codeZoneGeographique=" + codeZoneGeographique + ", libelleZoneGeographique="
				+ libelleZoneGeographique + "]";
	}
	
	
}
