package fr.els.conventioncollective.paie.model;

public class TypeFraisProfessionnel {

	private String codeFrais;
	
	private String libelleFrais;

	
	public TypeFraisProfessionnel() {
	}

	public TypeFraisProfessionnel(String codeFrais, String libelleFrais) {
		this.codeFrais = codeFrais;
		this.libelleFrais = libelleFrais;
	}

	public String getCodeFrais() {
		return codeFrais;
	}

	public void setCodeFrais(String codeFrais) {
		this.codeFrais = codeFrais;
	}

	public String getLibelleFrais() {
		return libelleFrais;
	}

	public void setLibelleFrais(String libelleFrais) {
		this.libelleFrais = libelleFrais;
	}

	@Override
	public String toString() {
		return "FraisProfessionnel [codeFrais=" + codeFrais + ", libelleFrais=" + libelleFrais + "]";
	}
	
}
