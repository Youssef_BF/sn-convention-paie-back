package fr.els.conventioncollective.paie.model;

public class Apprenti extends PaieCommon{

	
	private double anneeDebut;
	
	private double anneeFin;
	
	private double ageMin;
	
	private double ageMax;
	
	private NiveauEducationNatioanle niveauDebut;

	private NiveauEducationNatioanle niveauFin;
	
	private double pourcentageSmic;
	
	private double montantPourcentageSmic;
	
	private double pourcentageMinimaConventionnel;

	private double montantPourcentageMinimaConventionnel;

	private int idccParent;

	
	public double getAnneeDebut() {
		return anneeDebut;
	}

	public void setAnneeDebut(double anneeDebut) {
		this.anneeDebut = anneeDebut;
	}

	public double getAnneeFin() {
		return anneeFin;
	}

	public void setAnneeFin(double anneeFin) {
		this.anneeFin = anneeFin;
	}

	public double getAgeMin() {
		return ageMin;
	}

	public void setAgeMin(double ageMin) {
		this.ageMin = ageMin;
	}

	public double getAgeMax() {
		return ageMax;
	}

	public void setAgeMax(double ageMax) {
		this.ageMax = ageMax;
	}

	public NiveauEducationNatioanle getNiveauDebut() {
		return niveauDebut;
	}

	public void setNiveauDebut(NiveauEducationNatioanle niveauDebut) {
		this.niveauDebut = niveauDebut;
	}

	public NiveauEducationNatioanle getNiveauFin() {
		return niveauFin;
	}

	public void setNiveauFin(NiveauEducationNatioanle niveauFin) {
		this.niveauFin = niveauFin;
	}

	public double getPourcentageSmic() {
		return pourcentageSmic;
	}

	public void setPourcentageSmic(double pourcentageSmic) {
		this.pourcentageSmic = pourcentageSmic;
	}

	public double getMontantPourcentageSmic() {
		return montantPourcentageSmic;
	}

	public void setMontantPourcentageSmic(double montantPourcentageSmic) {
		this.montantPourcentageSmic = montantPourcentageSmic;
	}

	public double getPourcentageMinimaConventionnel() {
		return pourcentageMinimaConventionnel;
	}

	public void setPourcentageMinimaConventionnel(double pourcentageMinimaConventionnel) {
		this.pourcentageMinimaConventionnel = pourcentageMinimaConventionnel;
	}

	public double getMontantPourcentageMinimaConventionnel() {
		return montantPourcentageMinimaConventionnel;
	}

	public void setMontantPourcentageMinimaConventionnel(double montantPourcentageMinimaConventionnel) {
		this.montantPourcentageMinimaConventionnel = montantPourcentageMinimaConventionnel;
	}

	public int getIdccParent() {
		return idccParent;
	}

	public void setIdccParent(int idccParent) {
		this.idccParent = idccParent;
	}

	@Override
	public String toString() {
		return "Apprenti [anneeDebut=" + anneeDebut + ", anneeFin=" + anneeFin + ", ageMin=" + ageMin + ", ageMax="
				+ ageMax + ", niveauDebut=" + niveauDebut + ", niveauFin=" + niveauFin + ", pourcentageSmic="
				+ pourcentageSmic + ", montantPourcentageSmic=" + montantPourcentageSmic
				+ ", pourcentageMinimaConventionnel=" + pourcentageMinimaConventionnel
				+ ", montantPourcentageMinimaConventionnel=" + montantPourcentageMinimaConventionnel + ", idccParent="
				+ idccParent + "]";
	}

}
