package fr.els.conventioncollective.paie.model;

public class AppreciationAnciennete {
	
	private String code;

	private String libelle;

	
	public AppreciationAnciennete() {
	}

	public AppreciationAnciennete(String code, String libelle) {
		this.code = code;
		this.libelle = libelle;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@Override
	public String toString() {
		return "AppreciationAnciennete [code=" + code + ", libelle=" + libelle + "]";
	}
	
	
}
