package fr.els.conventioncollective.paie.model;

public class MaintienSalaire {
	
	private double duree;

	private double taux;

	private String specifites;
	
	private String travailEffectifCP;


	public MaintienSalaire() {
	}
	
	public MaintienSalaire(double duree, double taux, String specifites, String travailEffectifCP) {
		this.duree = duree;
		this.taux = taux;
		this.specifites = specifites;
		this.travailEffectifCP = travailEffectifCP;
	}


	public double getDuree() {
		return duree;
	}

	public void setDuree(double duree) {
		this.duree = duree;
	}

	public double getTaux() {
		return taux;
	}

	public void setTaux(double taux) {
		this.taux = taux;
	}

	public String getSpecifites() {
		return specifites;
	}

	public void setSpecifites(String specifites) {
		this.specifites = specifites;
	}

	public String getTravailEffectifCP() {
		return travailEffectifCP;
	}

	public void setTravailEffectifCP(String travailEffectifCP) {
		this.travailEffectifCP = travailEffectifCP;
	}

	@Override
	public String toString() {
		return "MaintienSalaire [duree=" + duree + ", taux=" + taux + ", specifites=" + specifites
				+ ", travailEffectifCP=" + travailEffectifCP + "]";
	}	
	
}
