package fr.els.conventioncollective.paie.model;

public class MoisVersementPrime {

	private int codeMois;
	
	private String libelle;

	
	public MoisVersementPrime() {
	}

	public MoisVersementPrime(int codeMois, String libelle) {
		this.codeMois = codeMois;
		this.libelle = libelle;
	}

	public int getCodeMois() {
		return codeMois;
	}

	public void setCodeMois(int codeMois) {
		this.codeMois = codeMois;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@Override
	public String toString() {
		return "MoisVersementPrime [codeMois=" + codeMois + ", libelle=" + libelle + "]";
	}
	
	
}
