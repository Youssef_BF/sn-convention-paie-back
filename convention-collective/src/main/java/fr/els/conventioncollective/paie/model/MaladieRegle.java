package fr.els.conventioncollective.paie.model;

public class MaladieRegle extends PaieCommon {

	private DroitConfondusSepares droitConfondusSepares;

	private ComptabilisationDroit comptabilisationDroit;

	private AppreciationAnciennete appreciationAnciennete;

	private EvolutionDroit evolutionDroit;

	private RegulAuNet regulAuNet;
	
	private ImpactCSG impactCSG;
	

	public DroitConfondusSepares getDroitConfondusSepares() {
		return droitConfondusSepares;
	}

	public void setDroitConfondusSepares(DroitConfondusSepares droitConfondusSepares) {
		this.droitConfondusSepares = droitConfondusSepares;
	}

	public ComptabilisationDroit getComptabilisationDroit() {
		return comptabilisationDroit;
	}

	public void setComptabilisationDroit(ComptabilisationDroit comptabilisationDroit) {
		this.comptabilisationDroit = comptabilisationDroit;
	}

	public AppreciationAnciennete getAppreciationAnciennete() {
		return appreciationAnciennete;
	}

	public void setAppreciationAnciennete(AppreciationAnciennete appreciationAnciennete) {
		this.appreciationAnciennete = appreciationAnciennete;
	}

	public EvolutionDroit getEvolutionDroit() {
		return evolutionDroit;
	}

	public void setEvolutionDroit(EvolutionDroit evolutionDroit) {
		this.evolutionDroit = evolutionDroit;
	}

	public RegulAuNet getRegulAuNet() {
		return regulAuNet;
	}

	public void setRegulAuNet(RegulAuNet regulAuNet) {
		this.regulAuNet = regulAuNet;
	}

	public ImpactCSG getImpactCSG() {
		return impactCSG;
	}

	public void setImpactCSG(ImpactCSG impactCSG) {
		this.impactCSG = impactCSG;
	}

	@Override
	public String toString() {
		return "MaladieRegle [droitConfondusSepares=" + droitConfondusSepares + ", comptabilisationDroit="
				+ comptabilisationDroit + ", appreciationAnciennete=" + appreciationAnciennete + ", evolutionDroit="
				+ evolutionDroit + ", regulAuNet=" + regulAuNet + ", impactCSG=" + impactCSG + "]";
	}
	

}
