package fr.els.conventioncollective.paie.model;

public class Decompte {

	private String codeDecompte;
	
	private String libelleDecompte;

	
	public Decompte() {
	}

	
	public Decompte(String codeDecompte, String libelleDecompte) {
		this.codeDecompte = codeDecompte;
		this.libelleDecompte = libelleDecompte;
	}


	public String getCodeDecompte() {
		return codeDecompte;
	}

	public void setCodeDecompte(String codeDecompte) {
		this.codeDecompte = codeDecompte;
	}

	public String getLibelleDecompte() {
		return libelleDecompte;
	}

	public void setLibelleDecompte(String libelleDecompte) {
		this.libelleDecompte = libelleDecompte;
	}

	@Override
	public String toString() {
		return "Decompte [codeDecompte=" + codeDecompte + ", libelleDecompte=" + libelleDecompte + "]";
	}
	

}
