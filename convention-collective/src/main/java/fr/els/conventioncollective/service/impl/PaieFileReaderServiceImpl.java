package fr.els.conventioncollective.service.impl;

import static fr.els.conventioncollective.constant.map.AppreciationAncienneteMap.getAppreciationAnciennete;
import static fr.els.conventioncollective.constant.map.ArretMaladieMap.getArret;
import static fr.els.conventioncollective.constant.map.CategorieProfessionelleMap.getCategorieProfessionnelle;
import static fr.els.conventioncollective.constant.map.ComptabilisationDroitMap.getComptabilisation;
import static fr.els.conventioncollective.constant.map.DroitConfonduSepareMap.getDroit;
import static fr.els.conventioncollective.constant.map.EvolutionDroitMap.getEvolutionDroit;
import static fr.els.conventioncollective.constant.map.FraisProfessionnelMap.getFraisProfessionnel;
import static fr.els.conventioncollective.constant.map.NiveauEducationNationaleMap.getNiveau;
import static fr.els.conventioncollective.constant.map.RegulAuNetMap.getRegulAuNet;
import static fr.els.conventioncollective.constant.map.TypeAncienneteMap.getTypeAnciennete;
import static fr.els.conventioncollective.constant.map.ZoneDeFraisMap.getZone;
import static fr.els.conventioncollective.constant.map.ZoneGeographiqueMap.getZoneGeographique;
import static fr.els.conventioncollective.constant.map.CongeMap.getConge;
import static fr.els.conventioncollective.constant.map.AcquisitionMap.getAcquisition;
import static fr.els.conventioncollective.constant.map.DecompteMap.getDecompte;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import fr.els.conventioncollective.paie.model.Anciennete;
import fr.els.conventioncollective.paie.model.Apprenti;
import fr.els.conventioncollective.paie.model.Carence;
import fr.els.conventioncollective.paie.model.CategorieProfessionnelle;
import fr.els.conventioncollective.paie.model.CongesDivers;
import fr.els.conventioncollective.paie.model.ContratPro;
import fr.els.conventioncollective.paie.model.FraisProfessionnelle;
import fr.els.conventioncollective.paie.model.MaintienSalaire;
import fr.els.conventioncollective.paie.model.MaladieMaintienSalaire;
import fr.els.conventioncollective.paie.model.MaladieRegle;
import fr.els.conventioncollective.paie.model.MinimaSalaire;
import fr.els.conventioncollective.paie.model.PaieCC;
import fr.els.conventioncollective.paie.model.PaieCommon;
import fr.els.conventioncollective.paie.model.ZoneGeographique;
import fr.els.conventioncollective.service.PaieFileReaderService;

@Service
public class PaieFileReaderServiceImpl implements PaieFileReaderService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PaieFileReaderServiceImpl.class);
	
	private static final DataFormatter dataFormatter = new DataFormatter();
	
	public static final String SAMPLE_XLSX_FILE_PATH = "C:/Users/ext-ybenkhallouf/Desktop/ELS/paie/Bâtiment ouvrier_latest.xls";
	

	private static int count = 2;

	public PaieCC readFromXls() {
		LOGGER.info("start executing readFromXls method, file : " + SAMPLE_XLSX_FILE_PATH);
		PaieCC paieCC = new PaieCC();

		try (Workbook workbook = WorkbookFactory.create(new File(SAMPLE_XLSX_FILE_PATH))) {

			// Retrieving the number of sheets in the Workbook
			System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");

			// get sheets names
			workbook.forEach(e -> {
				System.out.println(e.getSheetName());
			});


			// set Ids pour PaieCC
			paieCC.setIdCC(workbook.getSheetAt(0).getRow(3).getCell(0).getStringCellValue());
			paieCC.setIdccIndexe((int) workbook.getSheetAt(0).getRow(3).getCell(1).getNumericCellValue());

			// traitement onglet 0 => minima salaire
			Collection<MinimaSalaire> minimaSalaires = constructMinimaSalaires(workbook.getSheetAt(0));

			// traitement onglet 1 => Frais Pro
			Collection<FraisProfessionnelle> fraisProfessionnelles = constructFraisProfessionnelle(workbook.getSheetAt(1));

			// traitement onglet 2 => Apprenti
			Collection<Apprenti> apprentis = constructApprenti(workbook.getSheetAt(2));

			// traitement onglet 3 => contrat pro
			Collection<ContratPro> contratPros = constructContratPro(workbook.getSheetAt(3));

			// traitement onglet 4 => maladieRegle
			Collection<MaladieRegle> maladieRegles = constructMaladieRegle(workbook.getSheetAt(4));

			// traitement onglet 4 => Maladie Maintien Salaire
			Collection<MaladieMaintienSalaire> maladieMaintienSalaires = constructMaladieMaintientSalaire(workbook.getSheetAt(4));
			
			// traitement onglet 4 => Carence
			Collection<Carence> carences = constructCarences(workbook.getSheetAt(4));
			
			// traitement onglet 5 => Congés divers
			Collection<CongesDivers> congesDivers = constructCongesDivers(workbook.getSheetAt(5));

			paieCC.setMinimaSalaire(minimaSalaires);
			paieCC.setFraisProfessionnelles(fraisProfessionnelles);
			paieCC.setApprenti(apprentis);
			paieCC.setContratPro(contratPros);
			paieCC.setMaladieRegle(maladieRegles);
			paieCC.setMaladieMaintienSalaire(maladieMaintienSalaires);
			paieCC.setCarence(carences);
			paieCC.setCongesDivers(congesDivers);
			
		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			e.printStackTrace();
		}
		LOGGER.info("end executing method readFromXls.");
		return paieCC;
	}


	private Collection<MinimaSalaire> constructMinimaSalaires(Sheet sheetMinimaSalaire) {
		Collection<MinimaSalaire> minimaSalaires = new ArrayList<>();
		LOGGER.info("début du traitement de l'onglet : " + sheetMinimaSalaire.getSheetName());

		for (int i = 3; i <= sheetMinimaSalaire.getLastRowNum(); i++) {
			try {
				MinimaSalaire minimaSalaire = new MinimaSalaire();
				Row row = sheetMinimaSalaire.getRow(i);

				if (row != null && !row.getCell(0).getStringCellValue().isEmpty()) {
					count = 2;
					constructPaieCommon(row, minimaSalaire);
					minimaSalaire.setCategoriesProfessionnelles(getCategoriesProfessionnelles(dataFormatter.formatCellValue(row.getCell(count++))));
					minimaSalaire.setPosition(dataFormatter.formatCellValue(row.getCell(count++)));
					minimaSalaire.setEchelon(row.getCell(count++).getNumericCellValue());
					minimaSalaire.setCoefficient(row.getCell(count++).getNumericCellValue());
					minimaSalaire.setBaseFixe(row.getCell(count++).getNumericCellValue());
					minimaSalaire.setPoint(row.getCell(count++).getNumericCellValue());
					minimaSalaire.setFormule(row.getCell(count++).getStringCellValue());
					minimaSalaire.setSalaireMensuelMininalBrut(row.getCell(count++).getNumericCellValue());
					minimaSalaire.setTauxHoraireMininal(row.getCell(count++).getNumericCellValue());
					minimaSalaire.setRemunerationAnnuelleGarantie(row.getCell(count++).getNumericCellValue());
					minimaSalaire.setDonneesConventionnellesNonModelisees(row.getCell(count++).getStringCellValue());
					
					minimaSalaires.add(minimaSalaire);
				}else {
					break;
				}
			} catch (Exception e) {
				// continue if an exception is occured in on given line => continue..
				LOGGER.error(String.format("an error is occured when mapping the row N°: %d to MinimaSalaire : ", i)
						+ e.getMessage());
				continue;
			}
		}
		LOGGER.info("fin du traitement de l'onglet : " + sheetMinimaSalaire.getSheetName());
		
		return minimaSalaires;
	}
	private Collection<FraisProfessionnelle> constructFraisProfessionnelle(Sheet sheetFraisPro){
		Collection<FraisProfessionnelle> fraisProfessionnelles = new ArrayList<>();
		LOGGER.info("début du traitement de l'onglet : " + sheetFraisPro.getSheetName());
		for (int i = 3; i <= sheetFraisPro.getLastRowNum(); i++) {
			try {
				FraisProfessionnelle fraisProfessionnelle = new FraisProfessionnelle();
				Row row = sheetFraisPro.getRow(i);

				if (row != null && !row.getCell(0).getStringCellValue().isEmpty()) {
					count = 2;
					constructPaieCommon(row, fraisProfessionnelle);
					fraisProfessionnelle.setCategoriesProfessionnelles(getCategoriesProfessionnelles(row.getCell(count++).getStringCellValue()));
					fraisProfessionnelle.setDebutTypeFraisProfessionnel(getFraisProfessionnel(getCode(row.getCell(count++).getStringCellValue())));
					fraisProfessionnelle.setFinTypeFraisProfessionnel(getFraisProfessionnel(getCode(row.getCell(count++).getStringCellValue())));
					fraisProfessionnelle.setDebutZoneDeFrais(getZone(getCode(row.getCell(count++).getStringCellValue())));
					fraisProfessionnelle.setFinZoneDeFrais(getZone(getCode(row.getCell(count++).getStringCellValue())));
					fraisProfessionnelle.setDebutAnciennete(row.getCell(count++).getNumericCellValue());
					fraisProfessionnelle.setFinAnciennete(row.getCell(count++).getNumericCellValue());
					fraisProfessionnelle.setValeur(row.getCell(count++).getNumericCellValue());
					fraisProfessionnelle.setCommentaire(row.getCell(count++).getStringCellValue());
					
					fraisProfessionnelles.add(fraisProfessionnelle);
				}else {
					break;
				}
			} catch (Exception e) {
				// continue if an exception is occured in on given line => continue..
				LOGGER.error(String.format("an error is occured when mapping the row N°: %d to FraisProfessionnelle : ", i)
						+ e.getMessage());
				continue;
			}
		}
		
		LOGGER.info("fin du traitement de l'onglet : " + sheetFraisPro.getSheetName());

		return fraisProfessionnelles;
	}
	private Collection<Apprenti> constructApprenti(Sheet apprentiSheet) {
		Collection<Apprenti> apprentis = new ArrayList<>();
		LOGGER.info("début du traitement de l'onglet : " + apprentiSheet.getSheetName());
		for (int i = 3; i <= apprentiSheet.getLastRowNum(); i++) {
			try {
				Apprenti apprenti = new Apprenti();
				Row row = apprentiSheet.getRow(i);
				if (row != null && !row.getCell(0).getStringCellValue().isEmpty()) {
					count = 2;
					constructPaieCommon(apprentiSheet.getRow(i), apprenti);
					apprenti.setAnneeDebut(row.getCell(count++).getNumericCellValue());
					apprenti.setAnneeFin(row.getCell(count++).getNumericCellValue());
					apprenti.setAgeMin(row.getCell(count++).getNumericCellValue());
					apprenti.setAgeMax(row.getCell(count++).getNumericCellValue());
					apprenti.setNiveauDebut(getNiveau(getCode(row.getCell(count++).getStringCellValue())));
					apprenti.setNiveauFin(getNiveau(getCode(row.getCell(count++).getStringCellValue())));
					apprenti.setPourcentageSmic(row.getCell(count++).getNumericCellValue());
					apprenti.setMontantPourcentageSmic(row.getCell(count++).getNumericCellValue());
					apprenti.setPourcentageMinimaConventionnel(row.getCell(count++).getNumericCellValue());
					apprenti.setMontantPourcentageMinimaConventionnel(row.getCell(count++).getNumericCellValue());
					apprenti.setDonneesConventionnellesNonModelisees(row.getCell(count++).getStringCellValue());
					apprenti.setIdccParent((int) row.getCell(count++).getNumericCellValue());
					
					
					apprentis.add(apprenti);
				}else {
					break;
				}
			} catch (Exception e) {
				// continue if an exception is occured on the curret line => continue..
				LOGGER.error(String.format("an error is occured when mapping the row N°: %d to Apprenti : ", i)
						+ e.getMessage());
				continue;
			}
		}

		LOGGER.info("fin dutraitement de l'onglet : " + apprentiSheet.getSheetName());
		return apprentis;
	}
	private Collection<ContratPro> constructContratPro(Sheet contratProSheet) {
		Collection<ContratPro> contratPros = new ArrayList<>();
		LOGGER.info("début du traitement de l'onglet : " + contratProSheet.getSheetName());
		for (int i = 3; i <= contratProSheet.getLastRowNum(); i++) {
			try {
				ContratPro contratPro = new ContratPro();
				Row row = contratProSheet.getRow(i);
				if (row != null && !row.getCell(0).getStringCellValue().isEmpty()) {
					count = 2;
					constructPaieCommon(row, contratPro);
					contratPro.setAnneeDebut(row.getCell(count++).getNumericCellValue());
					contratPro.setAnneeFin(row.getCell(count++).getNumericCellValue());
					contratPro.setAgeMin(row.getCell(count++).getNumericCellValue());
					contratPro.setAgeMax(row.getCell(count++).getNumericCellValue());
					contratPro.setNiveauDebut(getNiveau(getCode(row.getCell(count++).getStringCellValue())));
					contratPro.setNiveauFin(getNiveau(getCode(row.getCell(count++).getStringCellValue())));
					contratPro.setPourcentageSMIC(row.getCell(count++).getNumericCellValue());
					contratPro.setPourcentageMinimaConventionnel(row.getCell(count++).getNumericCellValue());
					contratPro.setIdccParent((int) row.getCell(count++).getNumericCellValue());
					contratPro.setDonneesConventionnellesNonModelisees(row.getCell(count++).getStringCellValue());
					
					
					contratPros.add(contratPro);
				}else {
					break;
				}
			} catch (Exception e) {
				// continue if an exception is occured on the curret line => continue..
				LOGGER.error(String.format("an error is occured when mapping the row N°: %d to Contrat de Pro : ", i)
						+ e.getMessage());
				continue;
			}
		}

		LOGGER.info("fin dutraitement de l'onglet : " + contratProSheet.getSheetName());
		return contratPros;
	}

	private Collection<MaladieRegle> constructMaladieRegle(Sheet maladieRegleSheet) {
		Collection<MaladieRegle> maladieRegles = new ArrayList<>();
		LOGGER.info("début du traitement de l'onglet : " + maladieRegleSheet.getSheetName());
		for (int i = 3; i <= 3; i++) {
//		for (int i = 3; i < maladieRegleSheet.getLastRowNum(); i++) {  // FIXME
			try {
				MaladieRegle maladieRegle = new MaladieRegle();
				Row row = maladieRegleSheet.getRow(i);
				if (row != null && !row.getCell(0).getStringCellValue().isEmpty()) {
					count = 2;
					constructPaieCommon(row, maladieRegle);
					maladieRegle.setDroitConfondusSepares(getDroit(getCode(row.getCell(count++).getStringCellValue())));
					maladieRegle.setComptabilisationDroit(getComptabilisation(getCode(row.getCell(count++).getStringCellValue())));
					maladieRegle.setAppreciationAnciennete(getAppreciationAnciennete(getCode(row.getCell(count++).getStringCellValue())));
					maladieRegle.setEvolutionDroit(getEvolutionDroit(getCode(row.getCell(count++).getStringCellValue())));
					maladieRegle.setRegulAuNet(getRegulAuNet(getCode(row.getCell(count++).getStringCellValue())));
//					maladieRegle.setImpactCSG(getImpact(getCode(row.getCell(count++).getStringCellValue())));
					
					maladieRegles.add(maladieRegle);
				}else {
					break;
				}
			} catch (Exception e) {
				// continue if an exception is occured on the curret line => continue..
				LOGGER.error(String.format("an error is occured when mapping the row N°: %d to MaladieRegle : ", i)
						+ e.getMessage());
				continue;
			}
		}

		LOGGER.info("fin dutraitement de l'onglet : " + maladieRegleSheet.getSheetName());
		return maladieRegles;
	
	}


	private Collection<MaladieMaintienSalaire> constructMaladieMaintientSalaire(Sheet maintientSalaireSheet) {
		Collection<MaladieMaintienSalaire> maladieMaintienSalaires = new ArrayList<>();
		LOGGER.info("début du traitement de l'onglet : " + maintientSalaireSheet.getSheetName());
		for (int i = 7; i <= 37; i++) {
//		for (int i = 3; i < maintientSalaireSheet.getLastRowNum(); i++) {  // FIXME
			try {
				MaladieMaintienSalaire maladieMaintienSalaire = new MaladieMaintienSalaire();
				Row row = maintientSalaireSheet.getRow(i);
				if (row != null && !row.getCell(0).getStringCellValue().isEmpty()) {
					count = 2;
					constructPaieCommon(row, maladieMaintienSalaire);
					maladieMaintienSalaire.setCategorieProfessionnelles(getCategoriesProfessionnelles(dataFormatter.formatCellValue(row.getCell(count++))));
					maladieMaintienSalaire.setDebutTypeArret(getArret(getCode(row.getCell(count++).getStringCellValue())));
					maladieMaintienSalaire.setFinTypeArret(getArret(getCode(row.getCell(count++).getStringCellValue())));
					maladieMaintienSalaire.setDebutAge(row.getCell(count++).getNumericCellValue());
					maladieMaintienSalaire.setFinAge(row.getCell(count++).getNumericCellValue());
					maladieMaintienSalaire.setDebutDureeArret(row.getCell(count++).getNumericCellValue());
					maladieMaintienSalaire.setFinDureeArret(row.getCell(count++).getNumericCellValue());
					maladieMaintienSalaire.setAnciennete1(getAnciennete(row));
					maladieMaintienSalaire.setAnciennete2(getAnciennete(row));
					maladieMaintienSalaire.setMinEnfantAttendu((int) row.getCell(count++).getNumericCellValue());
					maladieMaintienSalaire.setMaxEnfantAttendu((int) row.getCell(count++).getNumericCellValue());
					maladieMaintienSalaire.setMinRangEnfant((int) row.getCell(count++).getNumericCellValue());
					maladieMaintienSalaire.setMaxRangEnfant((int) row.getCell(count++).getNumericCellValue());
					maladieMaintienSalaire.setMaintienSalaire1(getMaintientSalaire(row));
					maladieMaintienSalaire.setMaintienSalaire2(getMaintientSalaire(row));
					maladieMaintienSalaire.setIdccParent((int) row.getCell(count++).getNumericCellValue());
					maladieMaintienSalaire.setDonneesConventionnellesNonModelisees(row.getCell(count++).getStringCellValue());
	
					maladieMaintienSalaires.add(maladieMaintienSalaire);
				}else {
					break;
				}
			} catch (Exception e) {
				// continue if an exception is occured on the curret line => continue..
				LOGGER.error(String.format("an error is occured when mapping the row N°: %d to MaladieMaintienSalaire : ", i)
						+ e.getMessage());
				continue;
			}
		}

		LOGGER.info("fin dutraitement de l'onglet : " + maintientSalaireSheet.getSheetName());
		return maladieMaintienSalaires;
	
	}
	

	private Collection<Carence> constructCarences(Sheet carenceSheet) {
		Collection<Carence> carences = new ArrayList<>();
		LOGGER.info("début du traitement de l'onglet : " + carenceSheet.getSheetName());
		for (int i = 42; i <= 52; i++) {
//		for (int i = 3; i < carenceSheet.getLastRowNum(); i++) {  // FIXME
			try {
				Carence carence = new Carence();
				Row row = carenceSheet.getRow(i);
				if (row != null && !row.getCell(7).getStringCellValue().isEmpty()) { // TODO: we use cell N° 7 to check that line is note empty
					count = 2;
					constructPaieCommon(row, carence);
					carence.setCategorieProfessionnelles(getCategoriesProfessionnelles(dataFormatter.formatCellValue(row.getCell(count++))));
					carence.setDebutTypeArret(getArret(getCode(row.getCell(count++).getStringCellValue())));
					carence.setFinTypeArret(getArret(getCode(row.getCell(count++).getStringCellValue())));
					carence.setDebutDureeArret(row.getCell(count++).getNumericCellValue());
					carence.setFinDureeArret(row.getCell(count++).getNumericCellValue());
					carence.setDebutAnciennete(row.getCell(count++).getNumericCellValue());
					carence.setFinAnciennete(row.getCell(count++).getNumericCellValue());
					carence.setNombre(row.getCell(count++).getNumericCellValue());
					carence.setParticularites(row.getCell(count++).getStringCellValue());
					
					carences.add(carence);
				}else {
					break;
				}
			} catch (Exception e) {
				// continue if an exception is occured on the curret line => continue..
				LOGGER.error(String.format("an error is occured when mapping the row N°: %d to Carence : ", i)
						+ e.getMessage());
				continue;
			}
		}

		LOGGER.info("fin dutraitement de l'onglet : " + carenceSheet.getSheetName());
		return carences;
	
	}
	
	private Collection<CongesDivers> constructCongesDivers(Sheet congesDiversSheet) {
		Collection<CongesDivers> congesDivers = new ArrayList<>();
		LOGGER.info("début du traitement de l'onglet : " + congesDiversSheet.getSheetName());
		for (int i = 4; i < congesDiversSheet.getLastRowNum(); i++) { 
			try {
				CongesDivers congeDiver = new CongesDivers();
				Row row = congesDiversSheet.getRow(i);
				if (row != null && !row.getCell(7).getStringCellValue().isEmpty()) { // TODO: we use cell N° 7 to check that line is note empty
					count = 2;
					constructPaieCommon(row, congeDiver);
					congeDiver.setCategorieProfessionnelles(getCategoriesProfessionnelles(dataFormatter.formatCellValue(row.getCell(count++))));
					congeDiver.setDebutTypeConge(getConge(getCode(row.getCell(count++).getStringCellValue())));
					congeDiver.setFinTypeConge(getConge(getCode(row.getCell(count++).getStringCellValue())));
					congeDiver.setAncienneteMin(row.getCell(count++).getNumericCellValue());
					congeDiver.setAncienneteMax(row.getCell(count++).getNumericCellValue());
					congeDiver.setNombreJours(row.getCell(count++).getNumericCellValue());
					congeDiver.setPaiementConge("1".equals(getCode(row.getCell(count++).getStringCellValue())) ? "Oui" : "Non");
					congeDiver.setAcquisition(getAcquisition(getCode(row.getCell(count++).getStringCellValue())));
					congeDiver.setDecompte(getDecompte(getCode(row.getCell(count++).getStringCellValue())));
					congeDiver.setCommentaire(row.getCell(count++).getStringCellValue());
					
					congesDivers.add(congeDiver);
				}else {
					break;
				}
			} catch (Exception e) {
				// continue if an exception is occured on the curret line => continue..
				LOGGER.error(String.format("an error is occured when mapping the row N°: %d to CongesDivers : ", i)
						+ e.getMessage());
				continue;
			}
		}

		LOGGER.info("fin dutraitement de l'onglet : " + congesDiversSheet.getSheetName());
		return congesDivers;
	}

	
	private Anciennete getAnciennete(Row row) {
		Anciennete anciennete = new Anciennete();
		
		anciennete.setDebutAnciennete(row.getCell(count++).getNumericCellValue());
		anciennete.setFinAnciennete(row.getCell(count++).getNumericCellValue());
		anciennete.setTypeAnciennete(getTypeAnciennete(getCode(row.getCell(count++).getStringCellValue())));
		anciennete.setSpecifiteAnciennete(row.getCell(count++).getStringCellValue());
		
		return anciennete;
	}
	
	private MaintienSalaire getMaintientSalaire(Row row) {
		MaintienSalaire maintienSalaire = new MaintienSalaire();
		
		maintienSalaire.setDuree(row.getCell(count++).getNumericCellValue());
		maintienSalaire.setTaux(row.getCell(count++).getNumericCellValue());
		maintienSalaire.setSpecifites(row.getCell(count++).getStringCellValue());
		maintienSalaire.setTravailEffectifCP(row.getCell(count++).getStringCellValue());
		
		return maintienSalaire;
	}
	
	private void constructPaieCommon(Row row, PaieCommon paieCommon) {
		paieCommon.setDateFinRattachement(row.getCell(count++).getDateCellValue());
		paieCommon.setZonesGeographiques(getZonesGeographique(dataFormatter.formatCellValue(row.getCell(count++))));
//		paieCommon.setSecteurActivite(row.getCell(count++).getStringCellValue());
		paieCommon.setDateDebutValidite(row.getCell(count++).getDateCellValue());
		paieCommon.setDateFinValidite(row.getCell(count++).getDateCellValue());
		paieCommon.setAdherent("1".equals(getCode(row.getCell(count++).getStringCellValue())) ? "Oui" : "Non");
	}

	private Collection<ZoneGeographique> getZonesGeographique(String codeDept) {
		if (codeDept.isBlank()) {
			return null;
		}

		Collection<ZoneGeographique> zoneGeographiques = new ArrayList<>();
		String[] codeDepts = codeDept.split(",");

		for (int i = 0; i < codeDepts.length; i++) {
			zoneGeographiques.add(getZoneGeographique(codeDepts[i]));
		}
		return zoneGeographiques;
	}

	private Collection<CategorieProfessionnelle> getCategoriesProfessionnelles(String expression) {
		String[] codes = codesExtractor(expression);
		if (codes.length == 0) {
			return null;
		}
		Collection<CategorieProfessionnelle> categorieProfessionnelles = new ArrayList<>();

		for (int i = 0; i < codes.length; i++) {
			categorieProfessionnelles.add(getCategorieProfessionnelle(codes[i]));
		}
		return categorieProfessionnelles;
	}

	
	private String getCode(String expression) {
		return expression.isBlank() ? "" : expression.split("-")[0].strip();
	}
	
	private int[] codesExtractorInt(String expression) {
		String[] codeString;
		if (expression.contains(":")) {
			// => multiple codes
			codeString = expression.split(":")[0].strip().split("-");
		} else {
			// single code
			codeString = new String[] { expression.split("-")[0].strip() };
		}

		int[] codes = new int[codeString.length];
		var i = 0;
		for (String code : codeString) {
			codes[i] = Integer.parseInt(code);
			i++;
		}
		return codes;
	}

	private String[] codesExtractor(String expression) {
		if (expression.contains(":")) {
			// => multiple codes
			return expression.split(":")[0].strip().split("-");
		}
		// single code
		return new String[] { expression.split("-")[0].strip() };
	}
}
