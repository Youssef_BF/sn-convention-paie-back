package fr.els.conventioncollective.service;

import fr.els.conventioncollective.paie.model.PaieCC;

public interface PaieFileReaderService {

	public PaieCC readFromXls();
}
