package fr.els.conventioncollective.model;

import fr.els.conventioncollective.paie.model.PaieCC;
import fr.els.conventioncollective.prospection.prevoyence.model.ProspectionPrevoyence;

public class ConventionCollective {

	private int idCC;
	
	private PaieCC paieCC;
	
	private ProspectionPrevoyence prospectionPrevoyence;

	
	public int getIdCC() {
		return idCC;
	}

	public void setIdCC(int idCC) {
		this.idCC = idCC;
	}

	public PaieCC getPaieCC() {
		return paieCC;
	}

	public void setPaie(PaieCC paieCC) {
		this.paieCC = paieCC;
	}

	public ProspectionPrevoyence getProspectionPrevoyence() {
		return prospectionPrevoyence;
	}

	public void setProspectionPrevoyence(ProspectionPrevoyence prospectionPrevoyence) {
		this.prospectionPrevoyence = prospectionPrevoyence;
	}
	
}
