package fr.els.conventioncollective.controller.v1;


import fr.els.conventioncollective.paie.model.MinimaSalaire;


public interface PaieController {
	
	public MinimaSalaire getMinimSalaire(String idCC);
	
	
}
