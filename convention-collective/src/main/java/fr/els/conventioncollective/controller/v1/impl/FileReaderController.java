package fr.els.conventioncollective.controller.v1.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.els.conventioncollective.paie.model.CongesDivers;
import fr.els.conventioncollective.paie.model.PaieCC;
import fr.els.conventioncollective.repository.PaieCCRepository;
import fr.els.conventioncollective.service.impl.PaieFileReaderServiceImpl;


@RestController
@RequestMapping("/v1/els")
public class FileReaderController {

	private PaieFileReaderServiceImpl fileReaderService;
	private PaieCCRepository PaieCCRepository;
	private final MongoDbFactory mongo;

	@Autowired
	public FileReaderController(PaieFileReaderServiceImpl fileReaderService, PaieCCRepository PaieCCRepository, MongoDbFactory mongo) {
		this.fileReaderService = fileReaderService;
		this.PaieCCRepository = PaieCCRepository;
		this.mongo = mongo;
	}


	@GetMapping("/paieCC/MinimaSalaire")
	public Collection<CongesDivers> getMinimaSalaire() {
		PaieCCRepository.deleteAll();
		PaieCC paieCC = fileReaderService.readFromXls();
		
		PaieCCRepository.save(paieCC);
		
//		Collection<MinimaSalaire> minimaSalairesFromDB = PaieCCRepository.findAll().get(0).getMinimaSalaire();
//		minimaSalairesFromDB.forEach(e -> System.out.println("salaire min : " + e.getSalaireMensuelMininalBrut()));
		return PaieCCRepository.findByIdCC("Newidcc-10,Newidcc+10").getCongesDivers();

	}
	 
	
}
