package fr.els.conventioncollective.controller.v1.impl;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.els.conventioncollective.controller.v1.PaieController;
import fr.els.conventioncollective.paie.model.MinimaSalaire;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/v1/els/paie")
@Api(value = "/v1/els", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class PaieControllerImpl implements PaieController{


	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "OK"), 
			@ApiResponse(code = 404, message = "Mauvaise requête"), 
			@ApiResponse(code = 500, message = "Erreur interne serveur")
			})
	@GetMapping("/minimaSalaire/{idCC}")
	public MinimaSalaire getMinimSalaire(@ApiParam(value="idCC = id Convention Collective", required = true)  String idCC ) {
		// TODO Auto-generated method stub
		return null;
	}
}
