package fr.els.conventioncollective;

import java.io.IOException;
import java.net.ProxySelector;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

public class CallApiTest {

	private final static HttpClient HTTP_CLIENT = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2)
            .followRedirects(HttpClient.Redirect.NORMAL).proxy(ProxySelector.getDefault()).build();
    private final static HttpResponse.BodyHandler<String> asString = HttpResponse.BodyHandlers.ofString();

	public static void main(String[] args) throws IOException, InterruptedException {

        var HTTP_REQUEST = HttpRequest.newBuilder()
                .uri(URI.create( //Set the appropriate endpoint
                        new StringBuilder("http://10.8.41.22:8007/LATEST/resources/bdcctest?rs:idSyntheseToIDCC=Y5115")
                        .toString() ) )
                .timeout(Duration.ofMinutes(4000))
                .header("Content-Type", "application/json")
                .build();  
		var HTTP_RESPONSE = HTTP_CLIENT.send(HTTP_REQUEST, asString);

        // HTTP STATUS CODE 
        var statusCode = HTTP_RESPONSE.statusCode();
        System.out.println(statusCode);
	}

}
