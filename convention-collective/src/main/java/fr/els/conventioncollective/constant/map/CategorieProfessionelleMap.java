package fr.els.conventioncollective.constant.map;

import java.util.HashMap;
import java.util.Map;

import fr.els.conventioncollective.paie.model.CategorieProfessionnelle;

public abstract class CategorieProfessionelleMap {
	
	
	private CategorieProfessionelleMap() {
	}

	public static final Map<String, String> CATEGORIES_PROFESSIONELLES = new HashMap<String, String>();
	static  {
		CATEGORIES_PROFESSIONELLES.put("1", "Ouvrier");
		CATEGORIES_PROFESSIONELLES.put("2", "Employe");
		CATEGORIES_PROFESSIONELLES.put("3", "Technicien");
		CATEGORIES_PROFESSIONELLES.put("4", "Agent de Maitrise");
		CATEGORIES_PROFESSIONELLES.put("5", "Cadre");
		CATEGORIES_PROFESSIONELLES.put("6", "Chargé d'enquête");
		CATEGORIES_PROFESSIONELLES.put("7", "Agents");
		CATEGORIES_PROFESSIONELLES.put("8", "Assimilé cadre");
		CATEGORIES_PROFESSIONELLES.put("9", "Apprentis");
	 }
	
	public static CategorieProfessionnelle getCategorieProfessionnelle(String codeCategorie) {
		
		return new CategorieProfessionnelle(codeCategorie, CATEGORIES_PROFESSIONELLES.get(codeCategorie));
	}
	
}
