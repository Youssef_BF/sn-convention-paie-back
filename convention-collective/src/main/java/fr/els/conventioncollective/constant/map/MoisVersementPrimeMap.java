package fr.els.conventioncollective.constant.map;

import java.util.HashMap;
import java.util.Map;

import fr.els.conventioncollective.paie.model.MoisVersementPrime;

public abstract class MoisVersementPrimeMap {

	public static final Map<Integer, String> MOIS_VERSEMENT_PRIME = new HashMap<Integer, String>();

	private MoisVersementPrimeMap() {
	}
	
	static {
		MOIS_VERSEMENT_PRIME.put(1, "janvier A");
		MOIS_VERSEMENT_PRIME.put(2, "fevrier A");
		MOIS_VERSEMENT_PRIME.put(3, "mars A");
		MOIS_VERSEMENT_PRIME.put(4, "avril A");
		MOIS_VERSEMENT_PRIME.put(5, "mai A");
		MOIS_VERSEMENT_PRIME.put(6, "juin A");
		MOIS_VERSEMENT_PRIME.put(7, "juillet A");
		MOIS_VERSEMENT_PRIME.put(8, "aout A");
		MOIS_VERSEMENT_PRIME.put(9, "septembre A");
		MOIS_VERSEMENT_PRIME.put(10, "novembre A");
		MOIS_VERSEMENT_PRIME.put(11, "decembre A");
		MOIS_VERSEMENT_PRIME.put(12, "janvier A + 1");
		MOIS_VERSEMENT_PRIME.put(13, "fevrier A + 1");
		MOIS_VERSEMENT_PRIME.put(14, "mars A + 1");
		MOIS_VERSEMENT_PRIME.put(15, "avril A + 1");
		MOIS_VERSEMENT_PRIME.put(16, "mai A + 1");
		MOIS_VERSEMENT_PRIME.put(17, "juin A + 1");
		MOIS_VERSEMENT_PRIME.put(18, "juillet A + 1");
	}
	
	public static MoisVersementPrime getMoisVersement(int codeMois) {
		
		return new MoisVersementPrime(codeMois, MOIS_VERSEMENT_PRIME.get(codeMois));
	}
	
}
