package fr.els.conventioncollective.constant.map;

import java.util.HashMap;
import java.util.Map;

import fr.els.conventioncollective.paie.model.Acquisition;

public class AcquisitionMap {


	public static final Map<String, String> ACQUISITION= new HashMap<String, String>();

	private AcquisitionMap() {
	}
	
	static {
		ACQUISITION.put("1", "date ouverture des droits CP");
		ACQUISITION.put("2", "travail effectif si indemnisation à 100%");
		ACQUISITION.put("3", "travail effectif si indemnisation (partielle ou totale)");
		ACQUISITION.put("4", "dans la limite de 1 an ininterrompue");
		ACQUISITION.put("5", "dans la limite de 2 ans ininterompue");
		ACQUISITION.put("99", "Non spécifié");
	}
	
	public static Acquisition getAcquisition(String codeAcquisition) {
		
		return new Acquisition(codeAcquisition, ACQUISITION.get(codeAcquisition));
	}
	

}
