package fr.els.conventioncollective.constant.map;

import java.util.HashMap;
import java.util.Map;

import fr.els.conventioncollective.paie.model.TypePrime;

public abstract class PrimeGeneriqueMap {

	public static final Map<Integer, String> TYPES_PRIMES = new HashMap<Integer, String>();

	private PrimeGeneriqueMap() {
	}
	
	static {
		TYPES_PRIMES.put(1, "Prime ancienneté");
		TYPES_PRIMES.put(2, "Prime de vacance");
		TYPES_PRIMES.put(3, "13ème mois");
		TYPES_PRIMES.put(4, "Prime tutorat");
	}
	
	public static TypePrime getPrime(int codePrime) {
		
		return new TypePrime(codePrime, TYPES_PRIMES.get(codePrime));
	}
	
}
