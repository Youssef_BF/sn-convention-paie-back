package fr.els.conventioncollective.constant.map;

import java.util.HashMap;
import java.util.Map;

import fr.els.conventioncollective.paie.model.ZoneDeFrais;

public abstract class ZoneDeFraisMap {

	public static final Map<String, String> ZONES_DE_FRAIS = new HashMap<String, String>();

	private ZoneDeFraisMap() {
	}
	
	static {
		ZONES_DE_FRAIS.put("1", "Zone 1");
		ZONES_DE_FRAIS.put("2", "Zone 2");
		ZONES_DE_FRAIS.put("3", "Zone 3");
		ZONES_DE_FRAIS.put("4", "Zone 4");
		ZONES_DE_FRAIS.put("5", "Zone 5");
		ZONES_DE_FRAIS.put("6", "Zone 6");
		ZONES_DE_FRAIS.put("7", "Zone 1a");
		ZONES_DE_FRAIS.put("8", "Zone 1b");
	}
	
	public static ZoneDeFrais getZone(String codeZone) {
		return new ZoneDeFrais(codeZone, ZONES_DE_FRAIS.get(codeZone));
	}
	
}
