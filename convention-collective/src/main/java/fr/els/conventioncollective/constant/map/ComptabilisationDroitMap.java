package fr.els.conventioncollective.constant.map;

import java.util.HashMap;
import java.util.Map;

import fr.els.conventioncollective.paie.model.ComptabilisationDroit;

public abstract class ComptabilisationDroitMap {

	public static final Map<String, String> COMPTABILISATION_DROIT = new HashMap<String, String>();

	
	public ComptabilisationDroitMap() {
	}

	static {
		COMPTABILISATION_DROIT.put("1", "Année civile");
		COMPTABILISATION_DROIT.put("2", "Sur 12 mois mobiles consécutifs");
		COMPTABILISATION_DROIT.put("3", "Sur une année individuelle (date d'entrée)");
		COMPTABILISATION_DROIT.put("4", "Fin d'arrêt");
		COMPTABILISATION_DROIT.put("99", "Non spécifié");
	}
	
	public static ComptabilisationDroit getComptabilisation(String codeComptabilisation) {

		return new ComptabilisationDroit(codeComptabilisation, COMPTABILISATION_DROIT.get(codeComptabilisation));
	}
}
