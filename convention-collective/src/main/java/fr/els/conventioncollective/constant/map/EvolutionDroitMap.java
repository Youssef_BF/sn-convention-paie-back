package fr.els.conventioncollective.constant.map;

import java.util.HashMap;
import java.util.Map;

import fr.els.conventioncollective.paie.model.EvolutionDroit;

public abstract class EvolutionDroitMap {

	private EvolutionDroitMap() {
	}

	public static final Map<String, String> EVOLUTION_DROIT = new HashMap<String, String>();
	
	static {
		EVOLUTION_DROIT.put("1", "Pas d'évolution de droit");
		EVOLUTION_DROIT.put("2", "Evolution des droits avec régularisation");
		EVOLUTION_DROIT.put("3", "Evolution des droits sans régularisation");
	}
	
	public static EvolutionDroit getEvolutionDroit(String codeEvolutionDroit) {
		
		return new EvolutionDroit(codeEvolutionDroit, EVOLUTION_DROIT.get(codeEvolutionDroit));
	}
}
