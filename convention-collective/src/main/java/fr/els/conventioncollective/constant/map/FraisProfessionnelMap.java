package fr.els.conventioncollective.constant.map;

import java.util.HashMap;
import java.util.Map;

import fr.els.conventioncollective.paie.model.TypeFraisProfessionnel;

public abstract class FraisProfessionnelMap {

	public static final Map<String, String> FRAIS_PROFESSIONNELS = new HashMap<String, String>();

	private FraisProfessionnelMap() {
	}
	
	static {
		FRAIS_PROFESSIONNELS.put("1", "Repas");
		FRAIS_PROFESSIONNELS.put("2", "Trajet");
		FRAIS_PROFESSIONNELS.put("3", "Transport");
	}
	
	public static TypeFraisProfessionnel getFraisProfessionnel(String codeFrais) {
		
		return new TypeFraisProfessionnel(codeFrais, FRAIS_PROFESSIONNELS.get(codeFrais));
	}
	
}
