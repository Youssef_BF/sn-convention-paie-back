package fr.els.conventioncollective.constant.map;

import java.util.HashMap;
import java.util.Map;

import fr.els.conventioncollective.paie.model.ZoneGeographique;

public abstract class ZoneGeographiqueMap {

	private ZoneGeographiqueMap() {
	}
	public static final Map<String, String> DEPARTEMENTS = new HashMap<String, String>();
	
	static {
		
		DEPARTEMENTS.put("1", "Ain");
		DEPARTEMENTS.put("2", "Aisne");
		DEPARTEMENTS.put("3", "Allier");
		DEPARTEMENTS.put("4", "Alpes de Haute-Provence");
		DEPARTEMENTS.put("5", "Hautes-Alpes");
		DEPARTEMENTS.put("6", "Alpes-Maritimes");
		DEPARTEMENTS.put("7", "Ardèche");
		DEPARTEMENTS.put("8", "Ardennes");
		DEPARTEMENTS.put("9", "Ariège");
		DEPARTEMENTS.put("10", "Aube");
		DEPARTEMENTS.put("11", "Aude");
		DEPARTEMENTS.put("12", "Aveyron");
		DEPARTEMENTS.put("13", "Bouches du Rhône");
		DEPARTEMENTS.put("14", "Calvados");
		DEPARTEMENTS.put("15", "Cantal");
		DEPARTEMENTS.put("16", "Charente");
		DEPARTEMENTS.put("17", "Charente Maritime");
		DEPARTEMENTS.put("18", "Cher");
		DEPARTEMENTS.put("19", "Corrèze");
		DEPARTEMENTS.put("2A", "Corse du Sud");
		DEPARTEMENTS.put("2B", "Haute-Corse");
		DEPARTEMENTS.put("21", "Côte d'Or");
		DEPARTEMENTS.put("22", "Côtes d'Armor");
		DEPARTEMENTS.put("23", "Creuse");
		DEPARTEMENTS.put("24", "Dordogne");
		DEPARTEMENTS.put("25", "Doubs");
		DEPARTEMENTS.put("26", "Drôme");
		DEPARTEMENTS.put("27", "Eure");
		DEPARTEMENTS.put("28", "Eure-et-Loir");
		DEPARTEMENTS.put("29", "Finistère");
		DEPARTEMENTS.put("30", "Gard");
		DEPARTEMENTS.put("31", "Haute-Garonne");
		DEPARTEMENTS.put("32", "Gers");
		DEPARTEMENTS.put("33", "Gironde");
		DEPARTEMENTS.put("34", "Hérault");
		DEPARTEMENTS.put("35", "Ille-et-Vilaine");
		DEPARTEMENTS.put("36", "Indre");
		DEPARTEMENTS.put("37", "Indre-et-Loire");
		DEPARTEMENTS.put("38", "Isère");
		DEPARTEMENTS.put("39", "Jura");
		DEPARTEMENTS.put("40", "Landes");
		DEPARTEMENTS.put("41", "Loir-et-Cher");
		DEPARTEMENTS.put("42", "Loire");
		DEPARTEMENTS.put("43", "Haute-Loire");
		DEPARTEMENTS.put("44", "Loire-Atlantique");
		DEPARTEMENTS.put("45", "Loiret");
		DEPARTEMENTS.put("46", "Lot");
		DEPARTEMENTS.put("47", "Lot-et-Garonne");
		DEPARTEMENTS.put("48", "Lozère");
		DEPARTEMENTS.put("49", "Maine-et-Loire");
		DEPARTEMENTS.put("50", "Manche");
		DEPARTEMENTS.put("51", "Marne");
		DEPARTEMENTS.put("52", "Haute-Marne");
		DEPARTEMENTS.put("53", "Mayenne");
		DEPARTEMENTS.put("54", "Meurthe-et-Moselle");
		DEPARTEMENTS.put("55", "Meuse");
		DEPARTEMENTS.put("56", "Morbihan");
		DEPARTEMENTS.put("57", "Moselle");
		DEPARTEMENTS.put("58", "Nièvre");
		DEPARTEMENTS.put("59", "Nord");
		DEPARTEMENTS.put("60", "Oise");
		DEPARTEMENTS.put("61", "Orne");
		DEPARTEMENTS.put("62", "Pas-de-Calais");
		DEPARTEMENTS.put("63", "Puy-de-Dôme");
		DEPARTEMENTS.put("64", "Pyrénées-Atlantiques");
		DEPARTEMENTS.put("65", "Hautes-Pyrénées");
		DEPARTEMENTS.put("66", "Pyrénées-Orientales");
		DEPARTEMENTS.put("67", "Bas-Rhin");
		DEPARTEMENTS.put("68", "Haut-Rhin");
		DEPARTEMENTS.put("69", "Rhône");
		DEPARTEMENTS.put("70", "Haute-Saône");
		DEPARTEMENTS.put("71", "Saône-et-Loire");
		DEPARTEMENTS.put("72", "Sarthe");
		DEPARTEMENTS.put("73", "Savoie");
		DEPARTEMENTS.put("74", "Haute-Savoie");
		DEPARTEMENTS.put("75", "Paris");
		DEPARTEMENTS.put("76", "Seine-Maritime");
		DEPARTEMENTS.put("77", "Seine-et-Marne");
		DEPARTEMENTS.put("78", "Yvelines");
		DEPARTEMENTS.put("79", "Deux-Sèvres");
		DEPARTEMENTS.put("80", "Somme");
		DEPARTEMENTS.put("81", "Tarn");
		DEPARTEMENTS.put("82", "Tarn-et-Garonne");
		DEPARTEMENTS.put("83", "Var");
		DEPARTEMENTS.put("84", "Vaucluse");
		DEPARTEMENTS.put("85", "Vendée");
		DEPARTEMENTS.put("86", "Vienne");
		DEPARTEMENTS.put("87", "Haute-Vienne");
		DEPARTEMENTS.put("88", "Vosges");
		DEPARTEMENTS.put("89", "Yonne");
		DEPARTEMENTS.put("90", "Territoire-de-Belfort");
		DEPARTEMENTS.put("91", "Essonne");
		DEPARTEMENTS.put("92", "Hauts-de-Seine");
		DEPARTEMENTS.put("93", "Seine-St-Denis");
		DEPARTEMENTS.put("94", "Val-de-Marne");
		DEPARTEMENTS.put("95", "Val-d'Oise");
	}
	
	public static ZoneGeographique getZoneGeographique(String codeDept) {
		
		return new ZoneGeographique(codeDept, DEPARTEMENTS.get(codeDept));
	}
}
