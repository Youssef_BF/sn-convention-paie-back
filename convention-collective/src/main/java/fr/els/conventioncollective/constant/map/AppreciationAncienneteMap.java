package fr.els.conventioncollective.constant.map;

import java.util.HashMap;
import java.util.Map;

import fr.els.conventioncollective.paie.model.AppreciationAnciennete;

public abstract class AppreciationAncienneteMap {

	public static final Map<String, String> APPRECIATION_ANCIENNETE = new HashMap<String, String>();

	private AppreciationAncienneteMap() {
	}
	
	static {
		APPRECIATION_ANCIENNETE.put("1", "Début d'arrêt");
		APPRECIATION_ANCIENNETE.put("2", "Début de mois");
		APPRECIATION_ANCIENNETE.put("3", "Fin de mois");
		APPRECIATION_ANCIENNETE.put("99", "Non spécifié");
	}
	
	public static AppreciationAnciennete getAppreciationAnciennete(String codeAppreciationAnc) {
		
		return new AppreciationAnciennete(codeAppreciationAnc, APPRECIATION_ANCIENNETE.get(codeAppreciationAnc));
	}
	
}
