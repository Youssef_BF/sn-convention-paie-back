package fr.els.conventioncollective.constant.map;

import java.util.HashMap;
import java.util.Map;

import fr.els.conventioncollective.paie.model.DroitConfondusSepares;

public abstract class DroitConfonduSepareMap {

	public static final Map<String, String> DROITS = new HashMap<String, String>();

	
	private DroitConfonduSepareMap() {
	}
	static {
		DROITS.put("1", "Les droits maladies, AT/MP, accidents du trajet et maternité sont séparés.");
		DROITS.put("2", "Les droits maladies et AT/MP sont confondus, les droits accidents du trajet et maternités sont séparés.");
		DROITS.put("3", "Les droits maladies, AT/MP et accidents du trajet sont confondus, les droits maternités sont séparés.");
		DROITS.put("4", "Les droits AT/MP et accidents du trajet sont confondus, les droits maladies et maternités sont séparés.");
		DROITS.put("5", "Les droits maladies et AT/MP sont confondus, les droits accidents du trajet et maternités sont confondus.");
		DROITS.put("6", "Les droits maladies et maternités sont confondus, les droits AT/MP et accidents du trajet sont confondus.");
		DROITS.put("7", "Les droits maladies et accidents du trajet sont confondus, les droits AT/MP et maternités sont séparés.");
		DROITS.put("8", "Les droits maladies, AT/MP, accidents du trajet et maternité sont confondus.");
		DROITS.put("9", "Les droits maladies et accidents du trajet sont confondus, les droits AT/MP et maternités sont confondus.");
		DROITS.put("10", "Les droits maladies et maternités sont confondus, les droits AT/MP et accidents du trajet sont séparés.");
		DROITS.put("11", "Les droits AT/MP et maternités sont confondus, les droits maladies et accidents du trajet sont séparés.");
		DROITS.put("12", "Les droits accidents du trajet et maternités sont confondus, les droits maladies et AT/MP sont séparés.");
		DROITS.put("13", "Les droits maladies, AT/MP et maternités sont confondus, les droits accidents du trajet sont séparés.");
		DROITS.put("14", "Les droits maladies, accidents du trajet et maternités sont confondus, les droits AT/MP sont séparés.");
		DROITS.put("15", "Les droits maternités, AT/MP et accidents du trajet sont confondus, les droits maladies sont séparés.");
		DROITS.put("99", "Non précisé.");
	}
	public static DroitConfondusSepares getDroit(String codeDroit) {

		return new DroitConfondusSepares(codeDroit, DROITS.get(codeDroit));
	}
}
