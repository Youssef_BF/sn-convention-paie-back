package fr.els.conventioncollective.constant.map;

import java.util.HashMap;
import java.util.Map;

import fr.els.conventioncollective.paie.model.NiveauEducationNatioanle;

public abstract class NiveauEducationNationaleMap {
	
	public static final Map<String, String> NIVEAU_EDUCATION_NATIONALE = new HashMap<String, String>();

	private NiveauEducationNationaleMap() {
	}
	
	static {
		NIVEAU_EDUCATION_NATIONALE.put("1", "Niveau 1 : DEA - DESS - Master");
		NIVEAU_EDUCATION_NATIONALE.put("2", "Niveau 2 : Licence - Maitrise");
		NIVEAU_EDUCATION_NATIONALE.put("3", "Niveau 3 - BTS - DUT - DEUG");
		NIVEAU_EDUCATION_NATIONALE.put("4", "Niveau 4 - Bac - Bac Pro");
		NIVEAU_EDUCATION_NATIONALE.put("5", "Niveau 5 - CAP - BEP");
	}
	
	public static NiveauEducationNatioanle getNiveau(String codeNiveau) {
		
		return new NiveauEducationNatioanle(codeNiveau, NIVEAU_EDUCATION_NATIONALE.get(codeNiveau));
	}
	
}
