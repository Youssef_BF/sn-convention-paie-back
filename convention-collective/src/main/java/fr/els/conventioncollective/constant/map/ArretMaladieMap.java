package fr.els.conventioncollective.constant.map;

import java.util.HashMap;
import java.util.Map;

import fr.els.conventioncollective.paie.model.ArretMaladie;

public abstract class ArretMaladieMap {

	public static final Map<String, String> ARRETS_MALADIE = new HashMap<String, String>();

	private ArretMaladieMap() {
	}

	static {
		ARRETS_MALADIE.put("1", "Maladie");
		ARRETS_MALADIE.put("2", "AT");
		ARRETS_MALADIE.put("3", "MP");
		ARRETS_MALADIE.put("4", "Maternité");
		ARRETS_MALADIE.put("5", "Paternité");
		ARRETS_MALADIE.put("6", "Accident de trajet");
	}

	public static ArretMaladie getArret(String codeArret) {

		return new ArretMaladie(codeArret, ARRETS_MALADIE.get(codeArret));
	}

}
