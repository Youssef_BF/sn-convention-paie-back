package fr.els.conventioncollective.constant.map;

import java.util.HashMap;
import java.util.Map;

import fr.els.conventioncollective.paie.model.Conge;

public abstract class CongeMap {

	public static final Map<String, String> CONGES = new HashMap<String, String>();

	private CongeMap() {
	}
	
	static {
		CONGES.put("1", "Mariage salarié");
		CONGES.put("2", "Mariage enfant");
		CONGES.put("3", "Naissance");
		CONGES.put("4", "Adoption");
		CONGES.put("5", "Décès conjoint");
		CONGES.put("6", "Décès enfant");
		CONGES.put("7", "Décés parents");
		CONGES.put("8", "Décès grands parents");
		CONGES.put("9", "Décés beaux-parents");
		CONGES.put("10", "Déces frère/sœur");
		CONGES.put("11", "Bapteme");
		CONGES.put("12", "Communion");
		CONGES.put("13", "Pacs");
		CONGES.put("14", "Décès concubin");
		CONGES.put("15", "Décès partenaire lié par un pacs");
		CONGES.put("16", "Communion");
		CONGES.put("17", "Communion");
		CONGES.put("18", "Communion");
		CONGES.put("20", "Congés ancienneté");
		CONGES.put("30", "Médaille du travail");
		CONGES.put("40", "Annonce Handicap de l'enfant");
	}
	
	public static Conge getConge(String codeConge) {
		
		return new Conge(codeConge, CONGES.get(codeConge));
	}
}
