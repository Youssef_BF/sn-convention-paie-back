package fr.els.conventioncollective.constant.map;

import java.util.HashMap;
import java.util.Map;

import fr.els.conventioncollective.paie.model.RegulAuNet;

public abstract class RegulAuNetMap {

	public static final Map<String, String> REGUL_AU_NET = new HashMap<String, String>();

	static {
		REGUL_AU_NET.put("1", "Oui pour tous les types d'absences");
		REGUL_AU_NET.put("2", "Non pour tous les types d'absences");
		REGUL_AU_NET.put("3", "Oui pour la maladie mais pas pour l'AT/MP et la maternité");
		REGUL_AU_NET.put("4", "Oui pour la maladie et l'AT/MP mais pas pour la maternité");
		REGUL_AU_NET.put("5", "Oui pour la maternité mais pas pour la maladie et l'AT/MP");
	}
	
	public static RegulAuNet getRegulAuNet(String codeRegulAuNet) {
		
		return new RegulAuNet(codeRegulAuNet, REGUL_AU_NET.get(codeRegulAuNet));
	}
}
