package fr.els.conventioncollective.constant.map;

import java.util.HashMap;
import java.util.Map;

import fr.els.conventioncollective.paie.model.Decompte;

public abstract class DecompteMap {

	public static final Map<String, String> DECOMPTE = new HashMap<String, String>();

	private DecompteMap() {
	}
	
	static {
		DECOMPTE.put("1", "ouvré");
		DECOMPTE.put("2", "ouvrable");
		DECOMPTE.put("99", "Non précisé");
	}
	
	public static Decompte getDecompte(String codeDecompte) {
		
		return new Decompte(codeDecompte, DECOMPTE.get(codeDecompte));
	}
	
}
