package fr.els.conventioncollective.constant.map;

import java.util.HashMap;
import java.util.Map;

import fr.els.conventioncollective.paie.model.TypeAnciennete;

public abstract class TypeAncienneteMap {

	public static final Map<String, String> TYPE_ANCIENNETE = new HashMap<String, String>();

	private TypeAncienneteMap() {
	}

	static {
		TYPE_ANCIENNETE.put("1", "Ancienneté entreprise");
		TYPE_ANCIENNETE.put("2", "Ancienneté profession");
	}

	public static TypeAnciennete getTypeAnciennete(String codeArret) {
		return new TypeAnciennete(codeArret, TYPE_ANCIENNETE.get(codeArret));
	}

}
