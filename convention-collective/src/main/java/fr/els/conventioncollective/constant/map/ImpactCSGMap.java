package fr.els.conventioncollective.constant.map;

import java.util.HashMap;
import java.util.Map;

import fr.els.conventioncollective.paie.model.ImpactCSG;

public abstract class ImpactCSGMap {

	private ImpactCSGMap() {
	}

	public static final Map<String, String> IMPACT_CSG = new HashMap<String, String>();

	static {
		IMPACT_CSG.put("1", "Oui sous déduction CSG/CRDS sur IJSS");
		IMPACT_CSG.put("2", "Oui sans déduction CSG/CRDS sur IJSS");
		IMPACT_CSG.put("3", "Non");
	}

	public static ImpactCSG getImpact(String codeImpact) {

		return new ImpactCSG(codeImpact, IMPACT_CSG.get(codeImpact));
	}
}
