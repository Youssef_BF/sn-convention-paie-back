package fr.els.conventioncollective.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import fr.els.conventioncollective.paie.model.PaieCC;

public interface PaieCCRepository extends MongoRepository<PaieCC, String> {

	public PaieCC findByIdCC(String idCC);

	public List<PaieCC> findAll();
}
